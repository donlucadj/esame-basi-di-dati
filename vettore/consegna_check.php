<?php
session_start();


if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}

if(isset($_POST['submit'])){
    $check_data="/^[0-9]{4,4}\-[0-9]{2,2}\-[0-9]{2,2}$/";
    $check_time="/^[0-9]{2,2}\:[0-9]{2,2}$/";
    $patter_percorso="/^[\w]+$/";
    require ("../database.php");
    if(preg_match($check_data,$_POST['data']) &&
        filter_var($_POST['dipendente'],FILTER_VALIDATE_INT) &&
       preg_match($check_time,$_POST['ora']) &&
        preg_match($patter_percorso,$_POST['percorso']))
    {

        $dipendente=$_POST['dipendente'];
        $data=$_POST['data'];
        $ora=$_POST['ora'];
        $percorso=$_POST['percorso'];


        $sql="INSERT INTO Consegna (Data, Ora, Percorso, Dipendente)
VALUES(?,?,?,?)";

        $stm=$connect->prepare($sql);
        $stm->bind_param('sssi',$data,$ora,$percorso,$dipendente);
        $res=$stm->execute();

        if($stm->affected_rows >0){

            echo "ok";
        }

        }



}


?>