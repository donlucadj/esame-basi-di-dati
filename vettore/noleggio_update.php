<?php
session_start();
require('../database.php');
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}

if(isset($_GET['prodotto_id']) && isset($_GET['cliente_id'])

) {


    if (filter_var($_GET['prodotto_id'], FILTER_VALIDATE_INT) &&
        filter_var($_GET['cliente_id'], FILTER_VALIDATE_INT)
    ) {
        $prodotto=$_GET['prodotto_id'];
        $cliente=$_GET['cliente_id'];
        $sql = " SELECT Cliente.Ragione_Sociale as cliente ,Cliente.ID as cliente_id, Prodotto_Generico.nome, 
       Tipologia.nome as Tipologia, Colore.nome as Colore, Dimensione.Forma , 
       Categoria.nome as Categoria, Dimensione.Misura,Qty_noleggiata, Qty_da_consegnare,  DATE_FORMAT(Data, '%d-%m-%Y') as Data
,Prodotto.id as prodotto_id,Ultimo_incr_decr
from Noleggio
           inner join Prodotto on Prodotto.id=Noleggio.Prodotto 
           inner join Cliente on Cliente.id=Noleggio.Cliente 
           inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto.Prodotto_Generico
           inner join Tipologia on Tipologia.id=Prodotto.Tipologia 
           inner join Colore on Colore.id=Prodotto.Colore
           inner join Dimensione on Dimensione.id=Prodotto.Dimensione 
           inner join Categoria on Categoria.id=Prodotto.Categoria
           WHERE Prodotto.id= ?  and Cliente.ID = ? ";




        $inventory_sql = $connect->prepare($sql);
        $inventory_sql->bind_param('ii',$prodotto,$cliente);
        $inventory_sql->execute();
        $results=$inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata
        $row=$results->fetch_assoc();

    }
}
else{
    header("Location: noleggio_view.php");
}
?>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='noleggio_update.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>
<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="clienti.php" class=" aside_menu  "> Clienti  </a></li>
                <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu active  "> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
<h1> Aggiornamento prodotto magazzino</h1>

<table class="table_main">
    <tr class='row_header'>


      <th>  Cliente</th>
        <th>Nome</th>
        <th>Tipologia</th>
        <th>Colore</th>
        <th>Forma</th>
        <th>Misura</th>
        <th>Categoria</th>
        <th>Qty noleggiata</th>
        <th> Qty_da_consegnare</th>
        <th> Data</th>
<th> Ultimo incr/decr</th>
    </tr>




    <tr class='row'>
        <td><?php  echo $row['cliente'];?></td>
        <td><?php  echo $row['nome'];?></td>
        <td><?php  echo $row['Tipologia'];?></td>
        <td><?php  echo $row['Colore'];?></td>
        <td><?php  echo $row['Forma'];?></td>
        <td><?php  echo $row['Misura'];?></td>
        <td><?php  echo $row['Categoria'];?></td>
        <td><?php  echo $row['Qty_noleggiata'];?></td>
        <td><?php  echo $row['Qty_da_consegnare'];?></td>
        <td><?php  echo $row['Data'];?></td>
        <td><?php  echo $row['Ultimo_incr_decr'];?></td>

    </tr>





</table>
<form method="post" action="noleggio_update_check.php">

    <label> Inserisci l'incremento /decremento</label>
    <input type="number" name="quantity"  value="">
    <input type="hidden" name="prodotto_id" value="<?php echo $prodotto ?>">
    <input type="hidden" name="cliente_id" value="<?php echo $cliente ?>">
    <input type="submit" name="submit">
</form>

    </section>

</body>



