<?php
session_start();
ini_set ('display_errors', 1);
ini_set ('display_startup_errors', 1);
error_reporting (E_ALL);
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}
if(isset($_POST['submit'])) {


    require("../database.php");




        if (filter_var($_POST['ritiro_cliente'], FILTER_VALIDATE_INT) &&
            filter_var($_POST['prodotto'], FILTER_VALIDATE_INT) &&
            filter_var($_POST['quantity'], FILTER_VALIDATE_INT) &&
            isset($_POST['tipo']) == "NOLEGGIO" || isset($_POST['tipo']) == 'LAVAGGIO'
        ) {

            $ritiro_cliente = $_POST['ritiro_cliente'];
            $prodotto = $_POST['prodotto'];
            $tipo = $_POST['tipo'];
            $quantity = $_POST['quantity'];
            if ($tipo == 'NOLEGGIO') {

                $sql = "SELECT * FROM `Ritiro_Cliente` 
    inner join Noleggio on Noleggio.Cliente=Ritiro_Cliente.Cliente
    inner join Prodotto on      Noleggio.Prodotto=Prodotto.id                      
                                                            
 and Noleggio.Prodotto= ?
where Ritiro_Cliente.id= ?;";
                $stm = $connect->prepare($sql);
                $stm->bind_param('ii', $prodotto, $ritiro_cliente);
                $stm->execute();
                $res = $stm->get_result();
                if ($res->num_rows == 0) {
                    echo "Prodotto non noleggiato dal cliente";

                } else {
                    $result = $res->fetch_assoc();
                    $quantity_max = $result['Qty_noleggiata'] - $result['Qty_da_consegnare'];


                    if ($quantity <= $quantity_max) {

                        $sql_prodotto_ritirato = "
INSERT INTO Prodotto_Ritirato(Quantità, Tipo, Ritiro_cliente,Prodotto)
VALUES(?,?,?,?)";
                        $stm_p_c = $connect->prepare($sql_prodotto_ritirato);
                        $stm_p_c->bind_param('isii', $quantity, $tipo, $ritiro_cliente,
                            $prodotto);
                        $stm_p_c->execute();
                        if ($stm_p_c->affected_rows != 1) {
                            throw new mysqli_sql_exception("errore insert");
                        }


                        show_success();


                    } else {

                        echo "Quantità noleggiata non disponibile";
                    }

                }
                // controllare se il prodotto è stato noleggiato e se la quanatita < di quella noleggiata

            } else if ($tipo == 'LAVAGGIO') {


                $sql_prodotto_ritirato = "
INSERT INTO Prodotto_Ritirato(Quantità, Tipo, Ritiro_cliente,Prodotto)
VALUES(?,?,?,?)";
                $stm_p_c = $connect->prepare($sql_prodotto_ritirato);
                $stm_p_c->bind_param('isii', $quantity, $tipo, $ritiro_cliente, $prodotto);
                $stm_p_c->execute();
                if ($stm_p_c->affected_rows != 1) {

                    echo "error";
                }

            }


        }// end filter


    
}

function show_success(){

  echo "<p> Inserimento ottenuto con successo </p>";

}


?>

