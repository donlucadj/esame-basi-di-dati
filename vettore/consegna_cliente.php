<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE')  ) {

    header("Location:login.php");
}

require('../database.php');

if (isset($_POST['submit'])){
if(filter_var($_POST['quantity'],FILTER_VALIDATE_INT)&&
    filter_var($_POST['prodotto_consegnato'],FILTER_VALIDATE_INT)) {

    $prodotto_consegnato = $_POST['prodotto_consegnato'];
    $quantity = $_POST['quantity'];

    $sql_general = "SELECT Tipo, impacchettamento_standard,consegna_cliente FROM `Prodotto_Consegnato`
    inner join Prodotto on Prodotto.id=Prodotto_Consegnato.Prodotto 
WHERE Prodotto_Consegnato.ID = ? ";
    $stm_general = $connect->prepare($sql_general);
    $stm_general->bind_param('i', $prodotto_consegnato);
    $stm_general->execute();
    $res_general = $stm_general->get_result();
    $general = $res_general->fetch_assoc();
    $tipo = $general['Tipo'];
    $standard=$general['impacchettamento_standard'];
    $consegna_cliente=$general['consegna_cliente'];

    $upate_sql=" UPDATE Prodotto_Consegnato 
        SET Quantità = ?, colli = ? WHERE ID = ?";
    $stm_update=$connect->prepare($upate_sql);
    $colli = ceil($quantity / $standard);
    $stm_update->bind_param("iii",$quantity,$colli,$prodotto_consegnato);

    if ($tipo == 'NOLEGGIO') {
        $sql_post = "select Qty_noleggiata from Prodotto_Consegnato
    inner join Consegna_Cliente on Consegna_Cliente.ID=Prodotto_Consegnato.Consegna_Cliente 
    inner join Noleggio on Noleggio.Prodotto=Prodotto_Consegnato.Prodotto and 
Noleggio.Cliente=Consegna_Cliente.Cliente  where   Prodotto_Consegnato.ID= ?  
";

        $stm = $connect->prepare($sql_post);
        $stm->bind_param('i',$prodotto_consegnato);
        $stm->execute();

        $stm->store_result();
        $stm->bind_result($quantity_max);
        $stm->fetch();




        if ( $stm->num_rows == 0 ){
            echo "errore";

        }

        if ($quantity <= $quantity_max ) {




            $stm_update->execute();
            if($stm_update) {
                echo "success";


            }else{
                echo "errore";
            }



        }else{

            echo "Quantità non disponibile al noleggio";
        }
    }else if($tipo= 'LAVAGGIO'){

        $stm_update->execute();

        if($stm_update) {
            echo "<p> Aggiornamento effettuato con successo! </p>";


        }else{
            echo "errore";

        }// end noleggio


    } // end filter_var


    ?>

    <script>


        if (url == null) {
            var url = new URL(location.href);
            url.searchParams.set('consegna_cliente',<?php echo $consegna_cliente ?>);
            window.history.replaceState('','',url);
        }

    </script>


    <?php







    $_GET['consegna_cliente']=$consegna_cliente;

    $_SERVER['REQUEST_METHOD']="GET";

}
}
if($_SERVER['REQUEST_METHOD']=='GET' && isset($_GET['prodotto_consegnato'])){


    if (filter_var($_GET['prodotto_consegnato'], FILTER_VALIDATE_INT)) {
        $prodotto_consegnato = $_GET['prodotto_consegnato'];
        $sql='DELETE FROM Prodotto_Consegnato WHERE ID = ? ';
        $stm=$connect->prepare($sql);
        $stm->bind_param('i',$prodotto_consegnato);
        $stm->execute();


        if($stm->affected_rows== 1){

            echo "<p> Prodotto Eliminato con successo </p>";




        }else{
            echo "<p> ERRORE! </p>";
        }


    }

    $part=explode('=',$_SERVER['HTTP_REFERER']);

    unset($_GET['prodotto_consegnato']);

$_GET['consegna_cliente']=intval($part[1]);


?>

<script>


        if (url == null) {
            var url = new URL(location.href);
            console.log(url);
            url.searchParams.delete("prodotto_consegnato");
            url.searchParams.set('consegna_cliente',<?php echo $part[1]?>);
            window.history.replaceState('','',url);
        }

    </script>


    <?php











}
if($_SERVER['REQUEST_METHOD']=='GET' && isset($_GET['consegna_cliente'])){

if (filter_var($_GET['consegna_cliente'], FILTER_VALIDATE_INT)) {

    $consegna_cliente = $_GET['consegna_cliente'];

    $sql = 'SELECT Consegna.Id as id_consegna,Cliente.Ragione_Sociale,Stato_consegna, Stato_chiusura,stato  FROM  Consegna_Cliente 
inner join Cliente on Cliente.ID=Consegna_Cliente.Cliente
inner join Consegna on Consegna_Cliente.Consegna=Consegna.ID
where Consegna_Cliente.ID = ?
';


    $inventory_sql = $connect->prepare($sql);
    $inventory_sql->bind_param('i', $consegna_cliente);
    $inventory_sql->execute();
    $results = $inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata
    $row = $results->fetch_assoc();

    $sql_clients = "SELECT Prodotto_Consegnato.id, Prodotto_Generico.nome, Tipologia.nome as tipologia ,Colore.nome  as colore,
Dimensione.Forma,Dimensione.Misura,Quantità,Tipo,colli
FROM `Prodotto_Consegnato`
inner join Consegna_Cliente on
Prodotto_Consegnato.Consegna_Cliente=Consegna_Cliente.ID
inner join Prodotto on Prodotto.id =Prodotto_Consegnato.Prodotto
inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto.Prodotto_Generico
inner join Cliente on Cliente.id=Consegna_Cliente.Cliente
inner join Tipologia on Tipologia.id=Prodotto.Tipologia
inner join Colore on Colore.id=Prodotto.Colore
inner join Dimensione on Dimensione.id=Prodotto.Dimensione
inner join Categoria on Categoria.id=Prodotto.Categoria
where Consegna_Cliente.Id =?";
    $clients = $connect->prepare($sql_clients);
    $clients->bind_param('i', $consegna_cliente);
    $clients->execute();
    $prodotti_res = $clients->get_result();

        ?>
        <head>


            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="stylesheet" href='../index.css'>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
            <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

            <link href="../index.css" rel="stylesheet">


            <link href="account.css" rel="stylesheet">
        </head>
        <body>


        <nav class="main_nav">
            <ul>
                <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
                <li> <a href="logout.php"> Log Out </a></li>
            </ul>
        </nav>
        <main>
            <aside>

                <nav class="aside_nav">
                    <ul>
                        <li> <a  href="clienti.php" class=" aside_menu  "> Clienti  </a></li>
                        <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                        <li> <a href="consegna.php" class="aside_menu active  "> Consegne</a></li>
                        <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                        <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
                    </ul>

                </nav>

            </aside>
            <section class="results">
                <h1> Consegna cliente detail</h1>
                                <a href="consegna_detail.php?consegna=<?php echo $row['id_consegna'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
                <?php

                if( $row['Stato_chiusura'] == 1   ){
                ?>
                <button> <a href="crea_prodotto_consegnato.php?consegna_cliente=<?php echo $consegna_cliente ?>">Inserisci Prodotto Consegnato</a> </button>
<?php
                }
if(isset($_GET['msg'])){

    if($_GET['msg']=='success'){

        ?>
        <p> Prodotto consegnato inserito con successo</p>
        <?php
    }
    if($_GET['msg']=='error'){

        ?>
        <p> Errore!</p>
        <?php
    }
    if($_GET['msg']=='err_ins'){

        ?>
        <p> Prodotto già inserito</p>
        <?php
    }
    if($_GET['msg']=='err_disp'){

        ?>
        <p>Quantità noleggiata non disponibile </p>
        <?php
    }


}

 ?>
                <script>

                    function sayHi() {
                        if (url == null) {
                            var url = new URL(location.href);
                            console.log(url);
                            url.searchParams.delete("msg");

                            window.history.replaceState('','',url);
                        }
                    }

                    setTimeout(sayHi, 1000);
                </script>

                <table class="table_main">
                    <tr class='row_header'>
                        <th> Cliente</th>
                        <th> Operazione</th>
                        <th> Stato Consegna</th>
                        <th> Legenda</th>
                    </tr>



                    <tr class='row'>
                        <td><?php  echo $row['Ragione_Sociale'];?></td>
                        <?php
                        /* Se non vi sono nessun prodotto non posso chiudere la consegna_cliente */
                        if($prodotti_res->num_rows  == 0){
                            ?>
                            <td>Nessuna</td>
                            <td> Aperta</td>
                            <td> E' necessario inserire almeno un prodotto consegnato</td>
                            <?php

                        }
                        if($row['Stato_consegna']== 0 & $row['Stato_chiusura'] == 1 && $row['stato'] == 1  && $prodotti_res->num_rows >0 )
                        {

                            ?>
                            <td> <a href="chiusura_consegna_cliente.php?consegna_cliente=<?php echo $consegna_cliente?>">Chiudi Consegna</a></td>
                            <td>Aperta</td>
                            <td>Chiudendo la consegna non è più possibile modificarla</td>
                            <?php
                        }

                        if($row['Stato_consegna']== 0 && $row['Stato_chiusura'] == 0 && $row['stato'] == 1)
                        {
                            ?>
                            <td>Nessuna</td>
                            <td> Chiuso</td>

                            <?php

                        }
                        if($row['Stato_consegna']== 0 & $row['Stato_chiusura'] == 0 && $row['stato'] == 0 ) {

                            ?>

                            <td> <a href="stato_consegnata.php?consegna_cliente=<?php echo $consegna_cliente?>"> Cambia stato consegna</a></td>
                            <td> Consegna non effettuata</td>
                            <?php
                        }
                        if($row['Stato_consegna']== 1 & $row['Stato_chiusura'] == 0 && $row['stato'] == 0 ) {

                            ?>

                            <td> Nessuna </td>
                            <td> effettuata</td>
                            <?php
                        }
                        ?>
                    </tr>

                    <?php

                    ?>

                </table>
                <table class="table_main">
                    <tr class="row_header">
                        <th>Nome</th>
                        <th>Tipologia</th>
                        <th>Colore</th>
                        <th>Forma</th>
                        <th>Misura</th>
                        <th> Quantità</th>
                        <th>Tipo</th>
                        <th> Colli</th>
<?php
if( $row['Stato_chiusura'] == 1){
    while($prodotto=$prodotti_res->fetch_assoc()){
        ?>
        <tr>
            <td><?php  echo $prodotto['nome'];?></td>
            <td><?php  echo $prodotto['colore'];?></td>
            <td><?php  echo $prodotto['tipologia'];?></td>
            <td class='total_product_value'><?php  echo $prodotto['Forma'];?></td>
            <td class='total_product_value'><?php  echo $prodotto['Misura'];?></td>
            <form method="POST"  action="<?php echo $_SERVER['PHP_SELF']?>" id="myform">


            <td ><input type="number" name="quantity" value="<?php  echo $prodotto['Quantità']?>"></td>
                <td class='total_product_value'><?php  echo $prodotto['Tipo'];?></td>
                <td class='total_product_value'><?php  echo $prodotto['colli'];?></td>
<td> <input  type="submit" name="submit" value="aggiorna"  ></td>
<input type="hidden"  name="prodotto_consegnato" value="<?php  echo $prodotto['id']?>">

        </form>
            <td><a href='consegna_cliente.php?prodotto_consegnato=<?php echo $prodotto['id'] ?>'>Elimina</a></td>
        </tr

        <?php

    }
    }else{







                        while($prodotto=$prodotti_res->fetch_assoc()){
                            ?>
                            <tr>
                                <td><?php  echo $prodotto['nome'];?></td>
                                <td><?php  echo $prodotto['colore'];?></td>
                                <td><?php  echo $prodotto['tipologia'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Forma'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Misura'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Quantità'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Tipo'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['colli'];?></td>
                            </tr>
                            <?php



                    }
        }
                    ?>
                </table>
        </body>
        </html>
        <?php

}
}
?>


