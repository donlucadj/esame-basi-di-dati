<?php

if (empty($_SESSION['log']) && $_SESSION['ruolo'] !='VETTORE'  ) {

    header("Location:login.php");

}
require("database.php");
if(isset($_POST['submit'])) {
    if (filter_var($_POST['prodotto'], FILTER_VALIDATE_INT) &&
        filter_var($_POST['id'], FILTER_VALIDATE_INT) &&
        filter_var($_POST['quantity'], FILTER_VALIDATE_INT)) {

        $prodotto = $_POST['prodotto'];
        $cliente = $_POST['id'];
        $quantity = $_POST['quantity'];

        $array = array();
        $sql_check_qty = "SELECT Magazzino, Disponibilità FROM Gestito WHERE Prodotto = ?";
        $controllo = $connect->prepare($sql_check_qty);
        $controllo->bind_param('i', $prodotto);
        $controllo->execute();

        $res_rows = $controllo->get_result();
        $control = false;


        if ($res_rows->num_rows == 1) {
            $rows = $res_rows->fetch_assoc();
            $disp = $rows['Disponibilità'];
            $magazzino = $rows['Magazzino'];

            if ($quantity < $disp) {
                $array[0] = array("magazzino_id" => $magazzino,
                    "Disponibilità" => $disp
                );

                transaction($cliente, $prodotto, $quantity, $array);

            }

        } else {
            /*  Se trovo un magazzino che abbia la capacità lo inserisco lì*/
            $rows = $res_rows->fetch_all(MYSQLI_ASSOC);

            foreach ($rows as $row) {
                if ($quantity <= $row['Disponibilità']) {
                    $disp = $row['Disponibilità'];
                    $magazzino = $row['Magazzino'];
                    $control = true;
                    break;

                }
            }
            if ($control) {
                $array[0] = array("magazzino_id" => $magazzino,
                    "Disponibilità" => $disp);
                transaction($cliente, $prodotto, $quantity, $array);
                echo "ok";
            } else {
                $sum = 0;


                for ($i = 0; $i < count($rows); $i++) {
                    $sum += $rows[$i]['Disponibilità'];

                    $array[$i] = array("magazzino_id" => $rows[$i]["Magazzino"],
                        "Disponibilità" => $rows[$i]["Disponibilità"],
                    );
                    /* In modo da evitare di aggiungere alla somma
                     più magazzini quando si è raggiunta la quantità stabilita  */
                    if ($quantity <= $sum) {

                        break;
                    }

                }

                if ($quantity <= $sum) {
                    print_r($array);
                    echo $quantity;
                    transaction($cliente, $prodotto, $quantity, $array);
                    echo "ok";

                } else {
                    echo "Quantità non disponibile in magazzino";


                }


            }


        }

    }
}

        function transaction($cliente, $prodotto, $quantity, $array)
        {

            $dsn = 'mysql:host=localhost; dbname=lavanderia';
            $username = 'root';
            $password = '';
            $connect = new mysqli("localhost", $username, $password, "lavanderia");
            if ($connect->connect_errno) {
                echo "Failed to connect to MySQL: " . $connect->connect_error;
                exit();

            }
            $connect->begin_transaction();
            try {

                $ultimo_inc = 0;
                $insert_sql = " INSERT INTO Noleggia (Cliente, Prodotto, Qty_noleggiata,
Qty_da_consegnare,Ultimo_incr_decr)
VALUES(?,?,?,?,?)";

                $insert = $connect->prepare($insert_sql);
                $insert->bind_param('iiiii', $cliente, $prodotto, $quantity,
                    $quantity, $ultimo_inc);
                $insert->execute();


                if ($insert->affected_rows != 1) {
                    echo "Prodotto noleggiato";
                    throw new mysqli_sql_exception("errore insert");
                }
                $update_sql = "UPDATE  Gestito  SET Disponibilità = Disponibilità - ? 
WHERE Prodotto = ? AND Magazzino = ? ";
                $update = $connect->prepare($update_sql);
                $count = $quantity;
                foreach ($array as $value) {


                    if ($count >= $value['Disponibilità']) {


                        $valore = $value['Disponibilità'];
                        $update->bind_param('iii',  $valore, $prodotto, $value['magazzino_id']);
                        $update->execute();
                        $count = $count - $value['Disponibilità'];
                    } else if ($count < $value['Disponibilità']) {

                        $update->bind_param('iii',  $count, $prodotto, $value['magazzino_id']);
                        $update->execute();

                    }


                    if ($update->affected_rows == 0) {
                        throw new mysqli_sql_exception("errore update");
                    }
                }
                $connect->commit();
                echo "<p> Noleggio inserito </p>";
            } catch (mysqli_sql_exception $exception) {
                $connect->rollback();

                throw $exception;
            }

        }







    ?>