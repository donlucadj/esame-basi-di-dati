<?php
session_start();
require("../database.php");

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE')  ) {

    header("Location:login.php");
}
if($_GET['consegna_cliente']) {
    if (filter_var($_GET['consegna_cliente'], FILTER_VALIDATE_INT)) {
        $id = $_GET['consegna_cliente'];

        $sql_consegna_clienti="
SELECT Consegna_Cliente.ID as id, concat(Data,' - ', Ora,' - ', Percorso, ' - ', Ragione_Sociale) 
    as consegna_cliente 
FROM `Consegna_Cliente` 
    inner join Consegna on Consegna.ID= Consegna 
inner join Cliente on Cliente.ID=Cliente 
where Consegna_Cliente.Stato_chiusura = 1";

        $consegna_clienti=$connect->query($sql_consegna_clienti);

        $sql_prodotto="SELECT Prodotto.id, concat(Prodotto_Generico.nome ,'  ', Tipologia.nome , '   ', Categoria.Nome,'  ' , Colore.nome, '   ', Dimensione.Forma,'   ', Dimensione.Misura,'  ') as prodotto_consegnato FROM `Prodotto` 
    inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto_Generico inner join Tipologia on Tipologia.id=Tipologia inner join Colore on Colore.id=Colore inner join Dimensione on Dimensione.id=Dimensione 
    inner join Categoria on Categoria.id=Categoria;";
        $prodotto=$connect->query($sql_prodotto);



?>
<html>
<head>
    <link rel="stylesheet" href="product_detail.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
    <link href="prodotto_consegnato.css" rel="stylesheet">



    <link href="account.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>

<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="clienti.php" class=" aside_menu  active "> Clienti  </a></li>
                <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu  "> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>

            </ul>

        </nav>

    </aside>
    <section class="results">
        <h1> Inserimento Prodotti da consegnare nella consegna</h1>
        <div class="form_wrapper">

        <form action="prodotti_consegnati_check.php" method="post">
            <?php
            $m = $prodotto->fetch_all(MYSQLI_ASSOC);

            ?>
                    <div class="products_wrapper">

          <select name="prodotto">
                <option  selected value=""> Seleziona prodotti</option>
                <?php




                foreach ($m as $values) {
                    ?>

                    <option value="<?php echo $values['id']?>"> <?php echo $values['prodotto_consegnato'] ?></option>

                    <?php
                }
                ?>
            </select>
            <select name="tipo" required>
                <option value="">  Seleziona Tipo di Servizio</option>
                <option value="LAVAGGIO"> Lavaggio</option>
                <option value="NOLEGGIO">Noleggio</option>


            </select>

            <label for="quantity"> Quantità</label>
            <input min="1" required type="number" id="quantity>" name="quantity">
                    </div>
            <?php

            }
       ?>


<input type="hidden" name="consegna_cliente" value="<?php  echo $id?>">
            <input type="submit" name="submit" required>
        </form>
        </div>
        <script src='https://code.jquery.com/jquery-3.5.1.js'></script>
        <script src="js/jquery.multi-select.js" type="text/javascript"></script>


        <?php

}
?>
