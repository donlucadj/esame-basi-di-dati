<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (empty($_SESSION['log'])) {

    header("Location:login.php");
}

require('../database.php');

if(isset($_GET['ritiro'])) {

    if (filter_var($_GET['ritiro'], FILTER_VALIDATE_INT)) {
        $ritiro = $_GET['ritiro'];

        $sql = 'SELECT   Ritiro.ID,Data, Ora, Percorso,Ritiro.stato as stato_ritiro, concat(Dipendente.nome, " ",Dipendente.cognome) as dipendente from Ritiro, Dipendente
where Dipendente.id=Dipendente AND Ritiro.ID = ? ';


        $inventory_sql = $connect->prepare($sql);
        $inventory_sql->bind_param('i', $ritiro);
        $inventory_sql->execute();
        $results = $inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata

        $row=$results->fetch_assoc();


        ?>
        <head>


            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="stylesheet" href='index.css'>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
            <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

            <link href="../index.css" rel="stylesheet">


            <link href="account.css" rel="stylesheet">
        </head>
        <body>


        <nav class="main_nav">
            <ul>
                <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
                <li> <a href="logout.php"> Log Out </a></li>
            </ul>
        </nav>
        <main>
            <aside>

                <nav class="aside_nav">
                    <ul>
                        <li> <a  href="clienti.php" class=" aside_menu  "> Clienti  </a></li>
                        <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                        <li> <a href="consegna.php" class="aside_menu active  "> Consegne</a></li>
                        <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                        <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
                    </ul>

                </nav>

            </aside>
            <section class="results">

                <h1> Dettagli ritiro</h1>
                <a href="ritiro.php"> <span class="material-icons return_back">arrow_back</span> </a>
               <?php
                if($row['stato_ritiro']==1){
            ?>
        <button> <a href="crea_ritiro_cliente.php?ritiro=<?php echo $ritiro ?>">Inserisci cliente</a> </button>
            <?php
        }
                ?>
                <table class="table_main">
                    <tr class='row_header'>
                        <th> Id </th>
                        <th> Data</th>
                        <th>Ora</th>
                        <th>Percorso</th>
                        <th>dipendente</th>


                    </tr>
                    <?php

//while ($row=$inventory_sql->fetch_assoc()){
                        ?>
                        <tr class='row'>
                            <td><?php  echo $row['ID'];?></td>
                            <td><?php echo  date("d/m/Y", strtotime($row['Data']))?></td>
                            <td><?php  echo $row['Ora'];?></td>
                            <td><?php  echo $row['Percorso'];?></td>
                            <td><?php  echo $row['dipendente'];?></td>
                        </tr>



                    <?php
                    $sql_clients="SELECT  Ritiro_Cliente.ID as ritiro_cliente_id,Cliente.Ragione_Sociale FROM Ritiro_Cliente 
inner join Cliente on Cliente.id= Cliente and Ritiro = ?";
                    $clients=$connect->prepare($sql_clients);
                    $clients->bind_param('i',$ritiro);
                    $clients->execute();
                    $clients_res=$clients->get_result();
                    ?>

                </table>
                <table class="table_main">
                    <tr class="row_header">
                        <th>Cliente </th>
                        <th> Dettagli</th>

                    </tr>
                    <?php

                    while($client=$clients_res->fetch_assoc()){
                        ?>
                        <tr>
                            <td> <?php echo $client['Ragione_Sociale'] ?></td>
                            <td> <a href="ritiro_cliente.php?ritiro_cliente=<?php echo $client['ritiro_cliente_id'] ?>">View</a></td>
                        </tr>

                        <?php

                    }
                    ?>
                </table>
                <script src='https://code.jquery.com/jquery-3.5.1.js'></script>
                <script src='index.js'> </script>
        </body>

        </table>
        </body>
        </html>
        <?php
    }
}
?>



