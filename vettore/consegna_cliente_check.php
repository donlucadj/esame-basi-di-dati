<?php
session_start();
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}


    if(isset($_POST['submit'])) {

        require("../database.php");


        $value = true;

        foreach ($_POST['clienti'] as $a) {


            if (!(is_numeric($a))) {
                $value = false;
            }
        }

        if ($value && filter_var($_POST['consegna'], FILTER_VALIDATE_INT)) {
            try {
                $connect->begin_transaction();
                $consegna = $_POST['consegna'];
                $clienti = $_POST['clienti'];
                $insert_sql = "INSERT INTO  Consegna_Cliente (Consegna, Cliente)
VALUES (?,?)";

                foreach ($clienti as $cliente) {
                    $res_clienti = $connect->prepare($insert_sql);
                    $res_clienti->bind_param('ii', $consegna, $cliente);
                    $res_clienti->execute();

                    if ($res_clienti->affected_rows != 1) {
                        throw new mysqli_sql_exception("errore update");
                    }
                }
            $connect->commit();

            }
            catch (mysqli_sql_exception $exception) {
                $connect->rollback();

                throw $exception;
                echo "<p> ERRORE! </p>";
            }
            $str="Location:consegna_detail.php?consegna=".$consegna."&&success=1";

header($str);
    }
    }

    ?>



