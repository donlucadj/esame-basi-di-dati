<?php
ini_set ('display_errors', 1);
ini_set ('display_startup_errors', 1);
error_reporting (E_ALL);
if (empty($_SESSION['log']) && $_SESSION['ruolo'] !='VETTORE'  ) {

    header("Location:login.php");
}

require('../database.php');
require ('transaction.php');
 if(isset($_GET['id']) && isset($_GET['stato'])&&
     isset($_GET['prodotto'])) {

     if (filter_var($_GET['id'], FILTER_VALIDATE_INT) &&
         filter_var($_GET['prodotto'], FILTER_VALIDATE_INT) &&
         $_GET['stato'] == 1 || $_GET['stato'] == 0) {
         $cliente = $_GET['id'];
         $stato = $_GET['stato'];
         $prodotto = $_GET['prodotto'];
         $array = array();
         if ($stato == 0) {
             $sql_check = "
 SELECT Qty_noleggiata,Qty_da_consegnare,Magazzino,Qty_Noleggiata_Totale,Disponibilità
 
 
 FROM `Noleggia` inner join Gestito on 
             Gestito.Prodotto=Noleggia.Prodotto 
            WHERE Noleggia.Cliente=?  and Noleggia.Prodotto=? ";
             $stm_check = $connect->prepare($sql_check);
             $stm_check->bind_param('ii', $cliente, $prodotto);
             $stm_check->execute();
             $control = false;
             $rows = $stm_check->get_result()->fetch_all(MYSQLI_ASSOC);
             $count = count($rows);
             print_r($rows);
             if ($count == 1) {
                 $row = $rows[0];

                 if ($row['Qty_noleggiata'] == $row['Qty_da_consegnare'] &&
                     ($row['Qty_noleggiata'] <= $row['Qty_Noleggiata_Totale'])
                     && $row['Qty_noleggiata'] != 0
                 ) {

                     echo " posso effettuare lo scarico";
                     $array[0] = array("magazzino_id" => $row['Magazzino'],
                         "Disponibilità" => $row['Qty_Noleggiata_Totale']
                     );
                     $quantity = -$row['Qty_noleggiata'];

                     transaction($cliente, $prodotto, $quantity, $array, $stato);

                 }
                 if ($row['Qty_noleggiata'] == $row['Qty_da_consegnare'] &&
                     $row['Qty_noleggiata'] == 0) {
                     $sql_change = " UPDATE Noleggia SET Stato = ?
WHERE Prodotto=? AND Cliente = ?";
                     $stm_change = $connect->prepare($sql_change);
                     $stm_change->bind_param('iii', $stato, $prodotto, $cliente);
                     $stm_change->execute();
                     if ($stm_change->affected_rows != 1) {

                         echo "Errore";


                     } else {

                         echo "Prodotto Sospeso";
                     }
                 }

             }
             if ($count > 1) {


                 for ($i = 0; $i < count($rows); $i++) {

                     if ($rows[$i]['Qty_noleggiata'] <= $rows[$i]['Qty_Noleggiata_Totale']) {
                         $disp = $rows[$i]['Qty_Noleggiata_Totale'];
                         $magazzino = $rows[$i]['Magazzino'];
                         $quantity = -$rows[$i]['Qty_noleggiata'];
                         $control = true;
                         break;

                     }

                 }


                 if ($control) {
                     echo "trovato un magazzino disponibile";
                     $array[0] = array("magazzino_id" => $magazzino,
                         "Disponibilità" => $disp);
                     echo "<br>";
                     print_r($array);
                     transaction($cliente, $prodotto, $quantity, $array, $stato);

                 } else {
                     $sum = 0;


                     for ($i = 0; $i < count($rows); $i++) {

                         $sum += $rows[$i]['Qty_Noleggiata_Totale'];
                         $array[$i] = array("magazzino_id" => $rows[$i]["Magazzino"],
                             "Disponibilità" => $rows[$i]["Qty_Noleggiata_Totale"],
                         );
                         $quantity = -$rows[$i]['Qty_noleggiata'];


                         // In modo da evitare di aggiungere alla somma
                         // più magazzini quando si è raggiunta la quantità stabilita
                         if (abs($quantity) <= $sum) {

                             break;
                         }

                     }

                     if (abs($quantity) < $sum) {
                         print_r($array);
                         transaction($cliente, $prodotto, $quantity, $array,$stato);
                         echo "Trovato la somma su più più magazzini";


                     } else {
                         echo "Quantità non presente nei magazzini";
                     }


                 }


             }
         }

         else {

             $sql_change = " UPDATE Noleggia SET Stato = ?
WHERE Prodotto=? AND Cliente = ?";
             $stm_change = $connect->prepare($sql_change);
             $stm_change->bind_param('iii', $stato, $prodotto, $cliente);
             $stm_change->execute();
             if ($stm_change->affected_rows != 1) {

                 echo "Errore";


             } else {

                 echo "Prodotto Riattivato";
             }
         }


     }
 }
