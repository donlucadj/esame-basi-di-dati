<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (empty($_SESSION['log']) && $_SESSION['ruolo'] !='AMMINISTRATORE' ) {

    header("Location:login.php");
}



require("../database.php");
if(isset($_GET['ritiro'])) {

if (filter_var($_GET['ritiro'], FILTER_VALIDATE_INT)) {
    $ritiro = $_GET['ritiro'];

    $sql_ritiro = "SELECT ID, concat(DATE_FORMAT(Data, '%d/%m/%Y'),' ',Ora, ' ', Percorso) 
    as ritiro FROM  Ritiro WHERE Ritiro.ID = ?
";
    $ritiri = $connect->prepare($sql_ritiro);

    $ritiri->bind_param('i', $ritiro);
    $ritiri->execute();
$ritiri->bind_result($id,$ritiro_info);
$ritiri->fetch();
$ritiri->close();
    $sql_cliente = "SELECT ID,Ragione_Sociale FROM Cliente
 WHERE ID  NOT IN  (SELECT Cliente from Ritiro_Cliente where Ritiro=?);
";
    $clienti_sql = $connect->prepare($sql_cliente);
$clienti_sql->bind_param('i',$ritiro);
$clienti_sql->execute();
$clienti=$clienti_sql->get_result();
?>
<html>

<head>
    <link rel="stylesheet" href="../magazzino.css">
    <link href="css/multi-select.css" media="screen" rel="stylesheet" type="text/css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>

<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="clienti.php" class=" aside_menu   "> Clienti  </a></li>
                <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu active  "> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
<body>
<a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
<h1> Inserimento Ritiro Clienti</h1>
<form action="ritiro_cliente_check.php" method="post">
    <?php


    ?>
    <input type="text" readonly  value="<?php echo $ritiro_info ?>" >
    <input type="hidden" name="ritiro" value="<?php echo $id  ?>">
    <?php

    ?>
    <select multiple name="clienti[]"  id="my-select" required>
        <option value="">  Seleziona Clienti</option>
        <?php

        while($cliente=$clienti->fetch_assoc()){
            ?>

            <option value="<?php echo $cliente['ID']?>"> <?php echo $cliente['Ragione_Sociale'] ?></option>
            <?php
        }

        ?>


    </select>



    <input type="submit" name="submit" required>
</form>

<script src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script src="js/jquery.multi-select.js" type="text/javascript"></script>
<script>
    $('#my-select').multiSelect();
</script>
</body>
</html>

<?php
}
}
    ?>