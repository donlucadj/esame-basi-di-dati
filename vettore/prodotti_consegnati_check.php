<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();
require("../database.php");

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE')  ) {

    header("Location:login.php");
}
if(isset($_POST['submit'])) {







        if(filter_var($_POST['consegna_cliente'],FILTER_VALIDATE_INT) &&
            filter_var($_POST['prodotto'],FILTER_VALIDATE_INT) &&
            filter_var($_POST['quantity'],FILTER_VALIDATE_INT) &&
            $_POST['tipo'] == "NOLEGGIO" || $_POST['tipo'] == 'LAVAGGIO') {

            $consegna_cliente = $_POST['consegna_cliente'];
            $prodotto = $_POST['prodotto'];
            $tipo = $_POST['tipo'];
            $quantity = $_POST['quantity'];



            if ($tipo == 'NOLEGGIO') {

                $sql = "SELECT  Qty_da_consegnare, impacchettamento_standard,Consegna_Cliente.Cliente
        FROM `Noleggio` 
            inner join Consegna_Cliente on Noleggio.Cliente=Consegna_Cliente.Cliente 
        inner join Prodotto on Noleggio.Prodotto=Prodotto.id
        where Consegna_Cliente.ID= ? and Prodotto.id = ?  ";


                $stm = $connect->prepare($sql);
                $stm->bind_param('ii', $consegna_cliente, $prodotto);
                $stm->execute();
                $res = $stm->get_result();


                if ($res->num_rows == 0) {
                    echo "Prodotto non noleggiato dal cliente";

                } else {


                    $result = $res->fetch_assoc();

                    $quantity_max = $result['Qty_da_consegnare'];

                    if ($quantity <= $quantity_max) {
                        $standard = $result['impacchettamento_standard'];
                        echo $quantity."<br>".$standard;

                        $colli = ceil($quantity / $standard);



                            $sql_prodotto_consegnato = "
        INSERT INTO Prodotto_Consegnato(Quantità, Tipo, Consegna_cliente,Prodotto,colli)
        VALUES(?,?,?,?,?)";
                            $stm_p_c = $connect->prepare($sql_prodotto_consegnato);
                            $stm_p_c->bind_param('isiii', $quantity, $tipo, $consegna_cliente, $prodotto,
                                $colli);
                            $stm_p_c->execute();
                            if ($stm_p_c->affected_rows != 1) {
        echo "Prodotto consegnato già inserito";
                                $str="Location:consegna_cliente.php?consegna_cliente=".$consegna_cliente."&&msg=err_ins";
                            }else{
                                echo "Prodotto Inserito correttamente";
                                $str="Location:consegna_cliente.php?consegna_cliente=".$consegna_cliente."&&msg=success";
                            }


                    } else {

                        echo "Quantità noleggiata non disponibile";
                        $str="Location:consegna_cliente.php?consegna_cliente=".$consegna_cliente."&&msg=err_disp";
                    }
                }
                // controllare se il prodotto è stato noleggiato e se la qunatita < di quella noleggiata

            } else if ($_POST['tipo'] == 'LAVAGGIO') {

                $sql = "SELECT impacchettamento_standard FROM Prodotto  WHERE id = ?";
                $colli_stm = $connect->prepare($sql);
                $colli_stm->bind_param('i', $prodotto);
                $colli_stm->execute();
                $res_colli = $colli_stm->get_result();

                $res = $res_colli->fetch_assoc();
                $standard = $res['impacchettamento_standard'];
                $colli = ceil($quantity / $standard);
                $sql_prodotto_consegnato = "
        INSERT INTO Prodotto_Consegnato(Quantità, Tipo, Consegna_cliente,Prodotto,colli)
        VALUES(?,?,?,?,?)";
                $stm_p_c = $connect->prepare($sql_prodotto_consegnato);
                $stm_p_c->bind_param('isiii', $quantity, $tipo, $consegna_cliente, $prodotto, $colli);
                $stm_p_c->execute();
                if ($stm_p_c->affected_rows != 1) {

                    echo "Prodotto Consegnato già inserito";
                    $str="Location:consegna_cliente.php?consegna_cliente=".$consegna_cliente."&&ms=success";

                }else{
                    echo "Prodotto Inserito correttamente";
                    $str="Location:consegna_cliente.php?consegna_cliente=".$consegna_cliente."&&msg=error";
                }

            }

       header($str) ;

        }





}


?>
