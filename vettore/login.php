<?php




session_start();
if($_SESSION['log'] && $_SESSION['ruolo']=="VETTORE"){

    header("Location:clienti.php");
}

$token=bin2hex(random_bytes(16));
$_SESSION['token']=$token;

?>
<html>

<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400&family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Nunito:wght@900&family=Poppins:ital,wght@0,100;0,200;0,400;1,100;1,200&family=Raleway:ital,wght@0,200;0,400;1,100;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="login.css">

</head>

<body>
    <div  class='admin_content'>
        <div class='admin_title'>
<h2> Vettore  login </h2>
</div>
  <form method="POST"  action="login_check.php">

   
      <label for="email"> Email:</label>
      <input type="text" id="email"  autocomplete="off " name="email" required >
      <label for="password"> Password: </label>
      <input type="password" id="password" name="password" required>
<input type="hidden" id="token" name="token" value= "<?php echo $token?>"  >
      <input type="submit" name="submit" value="Send">


  </form>
</div>

<div class='message_box message_error hidden  '>
<i class='fa fa-ban fa-2x'> </i>
<span class='message_text'>  </span>
<button class='button_error btn_message_exit'>
  <i class="fa fa-times fa-2x exit-button "></i>
</button>
</div>


</body>

</html>