<?php
session_start();
require ("../database.php");
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}
if(isset($_GET['consegna_cliente'])){


    if (filter_var($_GET['consegna_cliente'], FILTER_VALIDATE_INT)) {
        $consegna_cliente = $_GET['consegna_cliente'];

        $sql = "select  Quantità, Prodotto, Cliente, Prodotto_Consegnato.Tipo from  Consegna_Cliente 
    inner join Prodotto_Consegnato on Prodotto_Consegnato.Consegna_Cliente=Consegna_Cliente.ID 
    WHERE Consegna_Cliente= ? and Tipo='NOLEGGIO' ";
        $stm = $connect->prepare($sql);
        $stm->bind_param('i', $consegna_cliente);
        $stm->execute();
        $stm_res = $stm->get_result();
        $result = $stm_res->fetch_all(MYSQLI_ASSOC);
        $num_row = $stm_res->num_rows;
        $stm_res->close();
        if ($num_row == 0) {
            $stato = 1;
            $update_stato = "UPDATE Consegna_Cliente SET Stato_consegna = ?
where ID = ? ";

            $update_stato_stm = $connect->prepare($update_stato);
            $update_stato_stm->bind_param('ii', $stato, $consegna_cliente);
            $update_stato_stm->execute();
            if ($update_stato_stm->affected_rows != 1) {

                echo "<p> Errore</p>";

            } else {

                echo "<p> Success</p>";
            }
        } else {


            try {
                $connect->begin_transaction();
                $stato = 1;


                $update_stato = "UPDATE Consegna_Cliente SET Stato_consegna = ?
where ID = ? ";

                $update_stato_stm = $connect->prepare($update_stato);
                $update_stato_stm->bind_param('ii', $stato, $consegna_cliente);
                $update_stato_stm->execute();
                if ($update_stato_stm->affected_rows != 1) {
                    throw new mysqli_sql_exception("errore update");
                }


                $update_sql = "UPDATE Noleggia SET Qty_da_consegnare = Qty_da_consegnare - ?
WHERE Prodotto = ? AND Cliente = ? ";
                $stm_update = $connect->prepare($update_sql);

                for ($i = 0; $i < count($result); $i++) {
                    $row = $result[$i];

                    $stm_update->bind_param('iii', $row['Quantità'], $row['Prodotto'], $row['Cliente']);
                    $stm_update->execute();
                    if ($stm_update->affected_rows != 1) {
                        throw new mysqli_sql_exception("errore update");
                    }
                }


                $connect->commit();
                echo "<p>success </p>";


            } catch (mysqli_sql_exception $exception) {
                $connect->rollback();

                throw $exception;
            }


        }
        ?>
<a href="consegna_cliente.php?consegna_cliente=<?php echo $consegna_cliente ?>">Ritorna indietro</a>
<?php


    }

}
        ?>
