<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}

require('../database.php');
if(isset($_GET['consegna_cliente'])){
    if (filter_var($_GET['consegna_cliente'], FILTER_VALIDATE_INT)) {
        $consegna_cliente = $_GET['consegna_cliente'];

        $sql='DELETE FROM Consegna_Cliente WHERE ID = ? ';
        $stm=$connect->prepare($sql);
        $stm->bind_param('i',$consegna_cliente);
        $stm->execute();


        if($stm->affected_rows== 1){

            echo "<p> Consegna Cliente con successo </p>";




        }else{
            echo "<p>  Errore! Cancellare prima i Prodotti Consegnati </p>";
        }


    }

    $part=explode('=',$_SERVER['HTTP_REFERER']);

    unset($_GET['prodotto_consegnato']);

    $_GET['consegna_cliente']=intval($part[1]);


    ?>

    <script>


        if (url == null) {
            var url = new URL(location.href);
            console.log(url);
            url.searchParams.delete("prodotto_consegnato");
            url.searchParams.set('consegna_cliente',<?php echo $part[1]?>);
            window.history.replaceState('','',url);
        }

    </script>


    <?php













}
if(isset($_GET['consegna'])) {

    if (filter_var($_GET['consegna'], FILTER_VALIDATE_INT)) {
        $consegna = $_GET['consegna'];

        $sql = 'SELECT   Consegna.ID,Data, Ora, Percorso, Consegna.stato as stato_consegna, concat(Dipendente.nome, " ",Dipendente.cognome) as dipendente from Consegna, Dipendente
where Dipendente.id=Dipendente AND Consegna.ID = ? ';


        $inventory_sql = $connect->prepare($sql);
        $inventory_sql->bind_param('i', $consegna);
        $inventory_sql->execute();
        $results = $inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata
        $row=$results->fetch_assoc()



    ?>
<head>


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>
<body>


<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="clienti.php" class=" aside_menu   "> Clienti  </a></li>
                <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu active  "> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
        <?php
        if(isset($_GET['success'])){

            ?>
       <p>  Consegna Cliente inserita con successo!</p>
          <?php
        }

        ?>
        <h1> Dettagli Consegna</h1>

        <a href="consegna.php"> <span class="material-icons return_back">arrow_back</span> </a>
        <?php



        if($row['stato_consegna']==1){
            ?>
        <button> <a href="crea_consegna_cliente.php?consegna=<?php echo $consegna ?>">Inserisci cliente</a> </button>
            <?php
        }

        ?>


        <table class="table_main">
            <tr class='row_header'>
                <th> Id </th>
                <th> Data</th>
                <th>Ora</th>
                <th>Percorso</th>
                <th>dipendente</th>


            </tr>

                <tr class='row'>
                    <td><?php  echo $row['ID'];?></td>
                    <td><?php echo  date("d/m/Y", strtotime($row['Data']))?></td>
                    <td><?php  echo $row['Ora'];?></td>
                    <td><?php  echo $row['Percorso'];?></td>
                    <td><?php  echo $row['dipendente'];?></td>
                </tr>


            <?php
            $sql_clients="SELECT  Consegna_Cliente.ID as consegna_cliente_id,Cliente.Ragione_Sociale, colli_totali FROM Consegna_Cliente 
inner join Cliente on Cliente.id= Cliente and Consegna = ?";
            $clients=$connect->prepare($sql_clients);
            $clients->bind_param('i',$consegna);
            $clients->execute();
            $clients_res=$clients->get_result();
            ?>

        </table>
        <table class="table_main">
            <tr class="row_header">
                <th>Cliente </th>
                <th> Dettagli</th>
                <th>Colli</th>


            </tr>
            <?php

            while($client=$clients_res->fetch_assoc()){
            ?>
            <tr>
                <td> <?php echo $client['Ragione_Sociale'] ?></td>
                <td> <a href="consegna_cliente.php?consegna_cliente=<?php echo $client['consegna_cliente_id'] ?>">View</a></td>


                <?php
                if($client['colli_totali'] == 0){

                    ?>
                    <td> Consegna  non ancora chiusa </td>
                    <td> <a href="consegna_detail.php?consegna_cliente=<?php echo $client['consegna_cliente_id']?>">Elimina Consegna Cliente</a></td>
                  <?php
                }
                else{
                    ?>

                    <td><?php echo $client['colli_totali'] ?></td>
                    <?php
                }
                echo "<tr>";
                }


                ?>

        </table>
        <script src='https://code.jquery.com/jquery-3.5.1.js'></script>
        <script src='index.js'> </script>
</body>

    </table>
    </body>
    </html>
    <?php
}
}
?>



