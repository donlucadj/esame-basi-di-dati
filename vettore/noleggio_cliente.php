<?php
session_start();
   if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}
require('../database.php');
if(isset($_GET['id'])) {




    if (filter_var($_GET['id'], FILTER_VALIDATE_INT)) {
        $id = $_GET['id'];


        $sql_clients = "SELECT  Ragione_Sociale FROM Cliente Where id=?";
        $clients = $connect->prepare($sql_clients);
        $clients->bind_param('i', $id);
        $clients->execute();
        $clients_res = $clients->get_result();


?>
<html>
<head>


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">

</head>
<body>

<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="clienti.php" class=" aside_menu  "> Clienti  </a></li>
                <li> <a href="noleggio.php" class="aside_menu active "> Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu   "> Consegna</a></li>
                <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
        <button> <a href="crea_noleggio.php?cliente=<?php echo $id ?>">Nuovo Noleggio</a> </button>
        <h1> Cliente</h1>
        <a href="noleggio.php"> <span class="material-icons return_back">arrow_back</span> </a>
    <table class="table_main">
        <tr class='row_header'>

            <th> Ragione Sociale</th>



        </tr>

        <?php
        while($row=$clients_res->fetch_assoc()){

            ?>
            <tr class='row'>
                <td><?php  echo $row['Ragione_Sociale'];?></td>
            </tr>






            <?php
            }
            ?>
    </table>
        <?php
        $sql=" SELECT  Prodotto.id as prodotto_id ,Prodotto_Generico.nome,
       Tipologia.nome as Tipologia, Colore.nome as Colore, Dimensione.Forma ,
       Categoria.nome as Categoria, Dimensione.Misura,Qty_noleggiata, Qty_da_consegnare,  DATE_FORMAT(Data, '%d-%m-%Y') as Data
,Prodotto.id as prodotto_id,Ultimo_incr_decr,Stato
from Noleggio
           inner join Prodotto on Prodotto.id=Noleggio.Prodotto
 
           inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto.Prodotto_Generico
           inner join Tipologia on Tipologia.id=Prodotto.Tipologia
           inner join Colore on Colore.id=Prodotto.Colore
           inner join Dimensione on Dimensione.id=Prodotto.Dimensione
           inner join Categoria on Categoria.id=Prodotto.Categoria
           WHERE Noleggio.Cliente= ? ";

   $stm=$connect->prepare($sql);
   $stm->bind_param('i',$id);
   $stm->execute();
   $res_stm=$stm->get_result();
?>
      <table>
         <tr>
            <th>Nome</th>
            <th>Tipologia</th>
            <th>Colore</th>
            <th>Forma</th>
            <th>Misura</th>
            <th>Categoria</th>
            <th> Noleggiata</th>
            <th> Da consegnare</th>
            <th> Data</th>
            <th> Stato</th>
             <th> Cambia Stato</th>
             <th> Aggiornamento</th>
         </tr>
        <?php






        while($row=$res_stm->fetch_assoc()){
            $stato=null;
            if($row['Stato']==1){
                $stato=0;

            }else{
                $stato=1;
            }
            ?>
            <tr class='row'>
                <td><?php  echo $row['nome'];?></td>
                <td><?php  echo $row['Tipologia'];?></td>
                <td><?php  echo $row['Colore'];?></td>
                <td><?php  echo $row['Forma'];?></td>
                <td><?php  echo $row['Misura'];?></td>
                <td><?php  echo $row['Categoria'];?></td>
                <td><?php  echo $row['Qty_noleggiata'];?></td>
                <td><?php  echo $row['Qty_da_consegnare'];?></td>
                <td><?php  echo $row['Data'];?></td>
                <td><?php echo   ( $row['Stato'] == 1 )  ?  " Attivo" : "Sospeso"  ?></td>
                <?php
                if( $row['Qty_da_consegnare'] ==$row['Qty_noleggiata'] ){
                    ?>
                    <td> <a href="stato_noleggio.php?id=<?php echo $id.'&stato='.$stato.'&prodotto='.$row['prodotto_id']?>"> Cambia stato </a> </td>
                    <?php
                }
                else{
                    ?>
                    <td> Operazione non disponibile</td>
                    <?php
                }
                if($row['Stato'] == 1){
                    ?>
                    <td><a href="noleggio_update.php?cliente_id=<?php echo $id?>&prodotto_id=<?php echo $row['prodotto_id'] ?>">Aggiorna Quantità</a> </td>
                    <?php

                }
                else{
                    ?>
                    <td> Aggioranento non disponibile</td>
                    <?php
                }
                ?>


            </tr>


            <?php

        }
        ?>
      </table>
    </section>
        <?php
    }
}





