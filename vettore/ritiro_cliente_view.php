
<?php

require('database.php');
if(isset($_GET['ritiro'])) {

    if (filter_var($_GET['ritiro'], FILTER_VALIDATE_INT)) {
        $ritiro=$_GET['ritiro'];

        $sql = 'SELECT   Ritiro.ID,Data, Ora, Percorso,  concat(Dipendente.nome, " ",Dipendente.cognome) as dipendente from Ritiro, Dipendente
where Dipendente.id=Dipendente AND Ritiro.ID = ? ';




        $inventory_sql = $connect->prepare($sql);
        $inventory_sql->bind_param('i',$ritiro);
        $inventory_sql->execute();
        $results=$inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata


    }
}
else{
    header("Location: consegna_view.php");
}
?>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
</head>
<body>

<h1> Ritiro cliente detail</h1>
<table class="table_main">
    <tr class='row_header'>
        <th> Id </th>
        <th> Data</th>
        <th>Ora</th>
        <th>Percorso</th>
        <th>dipendente</th>


    </tr>
    <?php
    while($row=$results->fetch_assoc()){
//while ($row=$inventory_sql->fetch_assoc()){
        ?>
        <tr class='row'>
            <td><?php  echo $row['ID'];?></td>
            <td><?php echo  date("d/m/Y", strtotime($row['Data']))?></td>
            <td><?php  echo $row['Ora'];?></td>
            <td><?php  echo $row['Percorso'];?></td>
            <td><?php  echo $row['dipendente'];?></td>
        </tr>


        <?php


    }

    ?>
    <?php
    $sql_clients="SELECT  Ritiro_Cliente.ID as ritiro_cliente_id,Cliente.Ragione_Sociale FROM Ritiro_Cliente 
inner join Cliente on Cliente.id= Cliente and Ritiro = ?";
    $clients=$connect->prepare($sql_clients);
    $clients->bind_param('i',$ritiro);
    $clients->execute();
    $clients_res=$clients->get_result();
    ?>

</table>
<table class="table_main">
    <tr class="row_header">
        <th>Cliente </th>
        <th> Dettagli</th>
    </tr>
    <?php

    while($client=$clients_res->fetch_assoc()){
        ?>
        <tr>
            <td> <?php echo $client['Ragione_Sociale'] ?></td>
            <td> <a href="prodotti_ritirati_view.php?ritiro_cliente=<?php echo $client['ritiro_cliente_id'] ?>">View</a></td>
        </tr>

        <?php

    }
    ?>
</table>
<script src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script src='index.js'> </script>
</body>
