<?php
session_start();
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE')  ) {

    header("Location:login.php");
}

require ("../database.php");
if(isset($_POST['submit'])){
    $check_data="/^[0-9]{4,4}\-[0-9]{2,2}\-[0-9]{2,2}$/";
    $check_time="/^[0-9]{2,2}\:[0-9]{2,2}$/";
    $patter_percorso="/^[\w]+$/";

    if(preg_match($check_data,$_POST['data']) &&
        filter_var($_POST['dipendente'],FILTER_VALIDATE_INT) &&
       preg_match($check_time,$_POST['ora']) &&
        preg_match($patter_percorso,$_POST['percorso']))
    {

        $dipendente=$_POST['dipendente'];
        $data=$_POST['data'];
        $ora=$_POST['ora'];
        $percorso=$_POST['percorso'];


        $sql="INSERT INTO Ritiro (Data, Ora, Percorso, Dipendente)
VALUES(?,?,?,?)";

        $stm=$connect->prepare($sql);
        $stm->bind_param('sssi',$data,$ora,$percorso,$dipendente);
       $stm->execute();

        if($stm->affected_rows  != 1){

            echo "<p> ERRORE Ritiro con gli stessi valori già creato </p>";
        }
        else{
            echo "<p>Ritiro creato con successo! </p>";
        }
        ?>
        <a href="ritiro.php">Ritorna ai Ritiri  </a>
   <?php




        }



}


?>