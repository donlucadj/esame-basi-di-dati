<?php
include('../config.php');
session_start();
session_unset();

session_destroy();
$ses_params = session_get_cookie_params();
   
    $options = array(
        'expires' => time()-3600,
        'path'     => $ses_params['path'],
        'domain'   => $ses_params['domain'],

        'samesite' => $ses_params['samesite']);
        
        
      
setcookie('PHPSESSID','', $options);


header("Location:index.php");

?>