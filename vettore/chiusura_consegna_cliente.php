<?php
session_start();
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE')  ) {

    header("Location:login.php");
    exit();
}
require('../database.php');
if(isset($_GET['consegna_cliente'])) {

    if (filter_var($_GET['consegna_cliente'], FILTER_VALIDATE_INT)) {
        $consegna_cliente = $_GET['consegna_cliente'];
        $sql_colli = "Select sum(colli) as colli_totali from Prodotto_Consegnato 
where Consegna_Cliente=?";
        $stm_colli = $connect->prepare($sql_colli);
        $stm_colli->bind_param('i', $consegna_cliente);
        $stm_colli->execute();
        $res_colli = $stm_colli->get_result();
        $fetc_colli = $res_colli->fetch_assoc();
        $colli_totali = $fetc_colli['colli_totali'];
        $sql = 'UPDATE Consegna_Cliente  SET Stato_chiusura = 0,
                             colli_totali = ? WHERE ID= ?
';

        $inventory_sql = $connect->prepare($sql);
        $inventory_sql->bind_param('ii', $colli_totali, $consegna_cliente);
        $inventory_sql->execute();

        if ($inventory_sql->affected_rows == 1) {
            echo "<p>  Consegna chiusa</p>";

        } else {
            echo "<p>  Consegna già chiusa</p>";

        }


    }
}
?>