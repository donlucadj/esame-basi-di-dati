<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (empty($_SESSION['log'])) {

    header("Location:login.php");
}

require('../database.php');




$sql="SELECT Ritiro.ID,Data, Ora, Percorso, Ritiro.stato as stato_ritiro, sum(Ritiro_Cliente.Stato_chiusura)as num_stato,count(Ritiro_Cliente.ID)as count_cc from Ritiro left join Ritiro_Cliente on Ritiro_Cliente.Ritiro=Ritiro.ID WHERE Dipendente= ? group BY ID";
$id=$_SESSION['id'];
$inventory_sql=$connect->prepare($sql);
$inventory_sql->bind_param("i",$id);
$inventory_sql->execute();
$inventory=$inventory_sql->get_result();
//print_r($inventory_sql->fetch(PDO::FETCH_ASSOC));
?>
<head>


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>
<body>


<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="clienti.php" class=" aside_menu  "> Clienti  </a></li>
                <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu   "> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu active">Ritiro</a></li>
                <li> <a href="gestito.php" class="aside_menu ">Inventario</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
        <h1> Ritiri</h1>
        <button> <a href="crea_ritiro.php">Crea Ritiro</a> </button>
        <table class="table_main">
            <tr class='row_header'>
                <th> Id </th>
                <th> Data</th>
                <th>Ora</th>
                <th>Percorso</th>

                <th> Clienti</th>
                <th> Stato Ritiro</th>
            </tr>
            <?php
            while($row=$inventory->fetch_assoc()){

                ?>
                <tr class='row'>
                    <td><?php  echo $row['ID'];?></td>
                    <td><?php echo  date("d/m/Y", strtotime($row['Data']))?></td>
                    <td><?php  echo $row['Ora'];?></td>
                    <td><?php  echo $row['Percorso'];?></td>

                    <td> <a href='ritiro_detail.php?ritiro=<?php echo $row['ID']?>' >view </a> </td>
        <?php
                    if($row['count_cc'] >= $row['num_stato'] && $row['stato_ritiro'] == 1
                    && $row['num_stato']!=NULL){
                    ?>
                    <td> Ritiri Clienti ancora aperte</td>
                    <?php
                    }



                    if( $row['num_stato']== 0  && $row['stato_ritiro'] == 1
                        && $row['num_stato']!=NULL 
                    ){
                        ?>
                        <th> <a href="chiudi_ritiro.php?ritiro=<?php echo $row['ID']?>"> Chiudi Ritiro</a></th>
                        <?php
                    }


                    if($row['num_stato'] == 0  && $row['stato_ritiro'] == 0){
                        ?>
                        <td> Ritiri Chiusi</td>


                        <?php
                    }

                    if($row['num_stato']==NULL){

                        ?>
                        <td> Nessuna Consegna Clienti </td>
                        <?php
                    }

                    ?>



                </tr>


                <?php


            }

            ?>

           
        </table>
    </section>


