<?php

require ("../database.php");

if(isset($_GET['ritiro_cliente'])){


    if (filter_var($_GET['ritiro_cliente'], FILTER_VALIDATE_INT)) {
        $ritiro_cliente = $_GET['ritiro_cliente'];

        $sql = "select  Quantità, Prodotto, Cliente, Prodotto_Ritirato.Tipo from  Ritiro_Cliente 
    inner join Prodotto_Ritirato on Prodotto_Ritirato.Ritiro_Cliente=Ritiro_Cliente.ID 
    WHERE Ritiro_Cliente= ? and Tipo='NOLEGGIO' ";
        $stm=$connect->prepare($sql);
        $stm->bind_param('i',$ritiro_cliente);
        $stm->execute();
        $stm_res=$stm->get_result();
        $result=$stm_res->fetch_all(MYSQLI_ASSOC);
        $stm_res->close();



        try {
            $connect->begin_transaction();
            $stato = 1;


            $update_stato = "UPDATE Ritiro_Cliente SET Stato_ritiro = ?
where ID = ? ";

            $update_stato_stm = $connect->prepare($update_stato);
            $update_stato_stm->bind_param('ii', $stato, $ritiro_cliente);
            $update_stato_stm->execute();
            if ($update_stato_stm->affected_rows != 1) {
                throw new mysqli_sql_exception("errore update");
            }


            $update_sql = "UPDATE Noleggio SET Qty_da_consegnare = Qty_da_consegnare + ?
WHERE Prodotto = ? AND Cliente = ? ";
            $stm_update = $connect->prepare($update_sql);

            for($i=0;$i<count($result);$i++){
                $row=$result[$i];

                $stm_update->bind_param('iii', $row['Quantità'], $row['Prodotto'], $row['Cliente']);
                $stm_update->execute();
                if( $stm_update->affected_rows != 1){
                    throw new mysqli_sql_exception("errore update");
                }
            }



            $connect->commit();
            echo "<p>success </p>";


        }
        catch (mysqli_sql_exception $exception) {
            $connect->rollback();

            throw $exception;
        }


    }


}
?>
