<?php
session_start();
require("../database.php");

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='VETTORE'  )) {

    header("Location:login.php");

}
}
if($_GET['cliente']) {
    if (filter_var($_GET['cliente'], FILTER_VALIDATE_INT)) {
        $id = $_GET['cliente'];



       $sql="SELECT Prodotto.id, Prodotto_Generico.nome , Tipologia.nome as Tipologia, Colore.nome as colore, Dimensione.Forma, Dimensione.Misura FROM `Prodotto` inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto_Generico inner join Tipologia on Tipologia.id=Tipologia  inner join Colore on Colore.id=Colore inner join Dimensione on Dimensione.id=Dimensione WHERE EXISTS( SELECT Prodotto from Gestito WHERE Gestito.Prodotto=Prodotto.id) AND Prodotto.id NOT IN ( SELECT Prodotto from Noleggio where Cliente = ?);
       
 ";

        $stm_q = $connect->prepare($sql);
        $stm_q->bind_param('i',$id);
$stm_q->execute();
$stm=$stm_q->get_result();

      $sql_cliente = "SELECT Ragione_Sociale FROM Cliente WHERE id= ?";
        $cliente = $connect->prepare($sql_cliente);
        $cliente->bind_param('i',$id);
        $cliente->execute();
        $cliente->bind_result($ragione_sociale);
$cliente->fetch();



?>
<html>
<head>
    <link rel="stylesheet" href="product_detail.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
    <link rel="stylesheet" href="../cliente.css">

    <link href="account.css" rel="stylesheet">
</head>

<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="clienti.php" class=" aside_menu  active "> Clienti  </a></li>
                <li> <a href="noleggio.php" class="aside_menu "> Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu  "> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account">Ritiro</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>

            </ul>

        </nav>

    </aside>
    <section class="results">
        <h1> Nuovo Noleggio</h1>

        <form action="noleggio_check.php" method="post">

            <select name="prodotto" required>
                <?php

                while($row=$stm->fetch_assoc()){

                    ?>
                    <option value="<?php  echo $row['id']?>"> <?php  echo $row['nome']." ".$row['Tipologia']." ".
                            $row['colore']." ".$row['Forma']." ".$row['Misura']
                        ?> </option>
                    <?php

                }
                ?>
            </select>
<input type="text" readonly value="<?php echo $ragione_sociale ?>">
          <input type="hidden"  name='id' value="<?php echo $id?>">
           <input type="number" name="quantity" min="1" required>


            <input type="submit" name="submit" required>
        </form>
    </section>
</main>
</body>
</html>

<?php
    }
}
?>
