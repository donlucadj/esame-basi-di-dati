<?php

session_start();

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");

}

require('../database.php');


$sql='SELECT  * FROM Cliente';

$inventory_sql=$connect->query($sql);

?>

<html>
<head>


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">

</head>
<body>

<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="dipendenti.php" class=" aside_menu "> Dipendenti   </a></li>
                <li> <a href="prodotti.php" class="aside_menu  "> Prodotti </a></li>
                <li> <a href="inventario.php" class="aside_menu  "> Inventario</a></li>
                <li> <a href="noleggio.php" class="aside_menu account active"> Clienti e Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu account"> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account"> Ritiri</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">

        <h1> Clienti</h1>
        <table class="table_main">
            <tr class='row_header'>
                <th> Id </th>
                <th> Ragione Sociale</th>
                <th>Sigla</th>
                <th>Partita Iva</th>
                <th> Prodotti Noleggiati</th>
            </tr>
            <?php
            while($row=$inventory_sql->fetch_assoc()){

            ?>
            <tr class='row'>
                <td><?php  echo $row['ID'];?></td>

                <td><?php  echo $row['Ragione_Sociale'];?></td>
                <td><?php  echo $row['Sigla'];?></td>
                <td><?php  echo $row['Partita_iva'];?></td>
                <td> <a href="noleggio_cliente.php?id=<?php echo $row['ID'] ?>">Noleggio</a> </td>

                <?php
                }

                ?>


        </table>

    </section>
</main>
</body>
</html>


