<?php
session_start();
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");

}

require('../database.php');
if(isset($_GET['ritiro_cliente'])) {


    if (filter_var($_GET['ritiro_cliente'], FILTER_VALIDATE_INT)) {
        $ritiro_cliente=$_GET['ritiro_cliente'];

        $sql = 'SELECT Cliente.Ragione_Sociale,Stato_ritiro, Stato_chiusura,stato  
FROM  Ritiro_Cliente 
inner join Cliente on Cliente.ID=Ritiro_Cliente.Cliente
inner join Ritiro on Ritiro_Cliente.Ritiro=Ritiro.ID
where Ritiro_Cliente.ID =?
';




        $inventory_sql = $connect->prepare($sql);
        $inventory_sql->bind_param('i',$ritiro_cliente);
        $inventory_sql->execute();
        $results=$inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata
        $row=$results->fetch_assoc();

    }
}
else{
    header("Location: ritiro_view.php");
}
?>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='../index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>
<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a href="dipendenti.php" class=" aside_menu "> Dipendenti   </a></li>
                <li> <a href="prodotti.php" class="aside_menu  "> Prodotti </a></li>
                <li> <a href="inventario.php" class="aside_menu  "> Inventario</a></li>
                <li> <a href="noleggio.php" class="aside_menu account"> Clienti e Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu account active"> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account"> Ritiri</a></li>
            </ul>

        </nav>

    </aside>

    <section class="results">
<h1> Ritiro cliente detail</h1>
        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
<table class="table_main">
    <tr class='row_header'>
        <th> Cliente</th>
        <th> Operazione</th>
        <th> Stato Ritiro</th>

    </tr>
    <?php

    //while ($row=$inventory_sql->fetch_assoc()){
    ?>
    <tr class='row'>
        <td><?php  echo $row['Ragione_Sociale'];?></td>

        <?php

        if($results->num_rows  == 0){
            ?>
            <td>Nessuna</td>
            <td> Aperta</td>
            <?php
        }
        $num_row=$results->num_rows;

        if($row['Stato_ritiro']== 0 & $row['Stato_chiusura'] == 1 && $row['stato'] == 1 && $num_row >0  )
        {

            ?>
            <td> <a href="chiusura_ritiro_clienti.php?ritiro_cliente=<?php echo $ritiro_cliente?>">Chiudi Ritiro Cliente</a></td>
            <td>Aperta</td>
            <?php
        }

        if($row['Stato_ritiro']== 0 && $row['Stato_chiusura'] == 0 && $row['stato'] == 1)
        {
            ?>
            <td>Nessuna</td>
            <td> Chiuso</td>

            <?php

        }
        if($row['Stato_ritiro']== 0 && $row['Stato_chiusura'] == 0 && $row['stato'] == 0 ) {

            ?>

            <td> <a href="stato_ritiro.php?ritiro_cliente=<?php echo $ritiro_cliente?>"> Cambia stato consegna</a></td>
            <td> Consegna non effettuata</td>
            <?php
        }
        if($row['Stato_ritiro']== 1 && $row['Stato_chiusura'] == 0 && $row['stato'] == 0 ) {

            ?>

            <td> Nessuna </td>
            <td> effettuata</td>
            <?php
        }
        ?>

    </tr>




















    <?php
    $sql_clients="SELECT Prodotto_Generico.nome, Tipologia.nome as tipologia ,Colore.nome  as colore,
Dimensione.Forma,Dimensione.Misura,Quantità,Tipo
FROM `Prodotto_Ritirato`
inner join Ritiro_Cliente on
Prodotto_Ritirato.Ritiro_Cliente=Ritiro_Cliente.ID
inner join Prodotto on Prodotto.id =Prodotto_Ritirato.Prodotto
inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto.Prodotto_Generico
inner join Cliente on Cliente.id=Ritiro_Cliente.Cliente
inner join Tipologia on Tipologia.id=Prodotto.Tipologia
inner join Colore on Colore.id=Prodotto.Colore
inner join Dimensione on Dimensione.id=Prodotto.Dimensione
inner join Categoria on Categoria.id=Prodotto.Categoria
where Ritiro_Cliente =?";
    $clients=$connect->prepare($sql_clients);
    $clients->bind_param('i',$ritiro_cliente);
    $clients->execute();
    $prodotti_res=$clients->get_result();
    ?>

</table>
<table class="table_main">
    <tr class="row_header">
        <th>Nome</th>
        <th>Tipologia</th>
        <th>Colore</th>

        <th>Forma</th>
        <th>Misura</th>
        <th> Quantità</th>
        <th>Tipo</th>

    </tr>
    <?php

    while($prodotto=$prodotti_res->fetch_assoc()){
        ?>
        <tr>
            <td><?php  echo $prodotto['nome'];?></td>
            <td><?php  echo $prodotto['colore'];?></td>
            <td><?php  echo $prodotto['tipologia'];?></td>
            <td class='total_product_value'><?php  echo $prodotto['Forma'];?></td>
            <td class='total_product_value'><?php  echo $prodotto['Misura'];?></td>
            <td class='total_product_value'><?php  echo $prodotto['Quantità'];?></td>
            <td class='total_product_value'><?php  echo $prodotto['Tipo'];?></td
        </tr>

        <?php

    }
    ?>
</table>
<script src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script src='index.js'> </script>
</body>

