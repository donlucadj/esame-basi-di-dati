<?php

session_start();
require('../database.php');

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");
}


?>

<html xmlns="http://www.w3.org/1999/html">
<head>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
    <link rel="stylesheet" href="../cliente.css">

</head>

<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="dipendenti.php" class=" aside_menu "> Dipendenti   </a></li>
                <li> <a href="prodotti.php" class="aside_menu  "> Prodotti </a></li>
                <li> <a href="inventario.php" class="aside_menu active "> Inventario</a></li>
                <li> <a href="noleggio.php" class="aside_menu account"> Clienti e Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu account"> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account"> Ritiri</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
<h1> Inserimento dipendente </h1>
        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
</body>
<form action="dipendente_check.php" method="POST">
    <label for="nome"> Nome</label>
    <input type="text" id="nome"   name="nome" required>
    <label for="cognome"> Cognome</label>
    <input type="text" id="cognome" name="cognome" required>
    <label for="codice_fiscale">Codice Fiscale</label>
    <input type="text" id="codice_fiscale" name="codice_fiscale" minlength="11" maxlength="16" required>

    <select name="ruolo" required>
        <option value='' > Seleziona Ruolo</option>
        <option value="AMMINISTRATORE"> Amministratore</option>
        <option value="VETTORE"> Vettore</option>
        <option value="MAGAZZINIERE"> Magazziniere</option>
    </select>
    <label for="email">Email </label>
    <input type="text" id="email" name="email" required>
    <input type="submit"  name="submit" >

</form>
</section>
</html>
