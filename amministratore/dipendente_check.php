<?php

session_start();
require("../database.php");

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");

}
if(isset($_POST['submit'])){

    $emailRegex ='/^[\w \.]{2,30}@[\w]+\.[a-z]+$/';
    $pattern_lettere="/^[\w  ò è ì]+$/";
    $pattern_c_f="/^[A-Z 0-9]{11,16}$/";
    if( preg_match($pattern_lettere,$_POST['nome']) &&
        preg_match($pattern_lettere,$_POST['cognome'])&&

        preg_match($pattern_c_f,$_POST['codice_fiscale'])&&
    preg_match($emailRegex,$_POST['email'])
        ) {

        $nome = $_POST['nome'];
        $cognome = $_POST['cognome'];
        $codice_fiscale = $_POST['codice_fiscale'];
        $ruolo = $_POST['ruolo'];
        $email = $_POST['email'];
     $password = bin2hex(random_bytes(6));
    
        $password_hash=password_hash($password,PASSWORD_DEFAULT);
           

        $sql = "INSERT INTO Dipendente  (Nome,Cognome, codice_fiscale,email,password,ruolo) 
VALUES(?,?,?,?,?,?)";
        $smt = $connect->prepare($sql);
        $smt->bind_param('ssssss', $nome, $cognome, $codice_fiscale, $email,$password_hash, $ruolo);
        $smt->execute();
        if ($smt->affected_rows != 1) {

            echo "<p> Errore! Campi uguali !</p>";
        } else {
 $to = $_POST['email'];
        $subject = 'Nuova password';
$msg = " Salve,L'amministratore dell'azienda  l'ha delegato con ruolo " . $ruolo . "<br>" .
            "Credenziali Email:  " . $email . "  <br>password:  " . $password;

        if (mail($to, $subject, $msg)) {

            echo "Mail inviata con successo al nuovo dipendente";
        }

            echo "<p> Dipendente inserito con successo </p>";
        }
        ?>
        <a href='dipendenti.php'> Ritorna ai dipendenti </a>
        <?php
    }
        }
if(isset($_POST['edit'])){
if(filter_var($_POST['id'],FILTER_VALIDATE_INT)&& isset($_POST['ruolo']))
{

    $id=$_POST['id'];
    $ruolo=$_POST['ruolo'];

    $update=$connect->prepare("UPDATE  Dipendente SET ruolo = ? WHERE id = ?  ");
    $update->bind_param('si',$ruolo,$id);
    $update->execute();
if($update->affected_rows !=1){

    echo "<p>Errore </p>";
}
else {
echo "<p> Ruolo Cambiato non successo</p> ";
}
?>
<a href="dipendenti.php">Clicca per tornare indietro</a>
<?php


}


}
if(isset($_GET['dipendente']) && isset($_GET['stato'])){
    if (filter_var($_GET['dipendente'], FILTER_VALIDATE_INT) &&
        $_GET['stato'] == 1 || $_GET['stato']== 0)

    {
        $id = $_GET['dipendente'];
        $stato = $_GET['stato'];

$sql_stato="UPDATE Dipendente SET Stato = ? WHERE id=?";
$stm_stato=$connect->prepare($sql_stato);
$stm_stato->bind_param('ii',$stato,$id);
$stm_stato->execute();
if($stm_stato->affected_rows != 1){

echo "<p>ERRORE!<p>";
}else{
echo "<p>Stato Cambiato con successo</p>";
}
?>
        <a href='dipendenti.php'> Ritorna ai dipendenti </a>
        <?php

    }

}
?>