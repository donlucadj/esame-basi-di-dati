<?php
session_start();


if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");

}
?>
<html>
<head>

    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">

</head>
<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="dipendenti.php" class=" aside_menu active"> Dipendenti   </a></li>
                <li> <a href="prodotti.php" class="aside_menu  "> Prodotti </a></li>
                <li> <a href="inventario.php" class="aside_menu "> Inventario</a></li>
                <li> <a href="noleggio.php" class="aside_menu account"> Clienti e Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu account"> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account"> Ritiri</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
        <?php

        require('../database.php');

        $sql='SELECT  * FROM Dipendente';

        $inventory_sql=$connect->query($sql);

        ?>
     <button> <a href="crea_dipendente.php">Crea Dipendente</a> </button>
        <table class="table_main">
            <tr class='row_header'>
                <th> Id </th>
                <th> Nome</th>
                <th>Cognome</th>
                <th>Codice Fiscale</th>
                <th> Ruolo</th>
                <th>Stato</th>


            </tr>
            <?php
            while($row=$inventory_sql->fetch_assoc()){
            $stato=null;
            if($row['Stato']==1){
                $stato=0;

            }else{
                $stato=1;
            }
            ?>
            <tr class='row'>
                <td><?php  echo $row['id'];?></td>
                <td><?php  echo $row['Nome'];?></td>
                <td><?php  echo $row['Cognome'];?></td>
                <td><?php  echo $row['codice_fiscale'];?></td>
                <?php
                if(isset($_GET['edit']) && $_GET['edit']== $row['id'] && $row['Stato'] == 1 ){


                    ?>
                        <form action="dipendente_check.php" method="post">
                        <td>
                    <select name="ruolo" required>
                        <option value="" > Seleziona Ruolo</option>
                        <option value="AMMINISTRATORE" <?php echo $row['ruolo']== 'AMMINISTRATORE'? "selected":" "?>> Amministratore</option>
                        <option value="VETTORE"<?php echo $row['ruolo']== 'VETTORE'? "selected":" "?>> Vettore</option>
                        <option value="MAGAZZINIERE"<?php echo $row['ruolo']== 'MAGAZZINIERE'? "selected":" "?>> Magazziniere</option>
                    </select>
                        </td>
                    <td><?php echo   ( $row['Stato'] == 1 )  ?  " Attivo" : "Sospeso"  ?></td>
                            <input type="hidden" name="id" value="<?php echo $row['id'] ?>">
                            <td> <input type="submit" name="edit"> </td>

                        </form>
                    <td><a href="dipendenti.php"> Reset</a></td>
                    <?php
                }
                else{
                ?>
                <td><?php  echo $row['ruolo'];?></td>







                <td>  <a href="dipendente_check.php?dipendente=<?php echo $row['id'].'&stato='.$stato ?>"> <?php echo ( $row['Stato'] == 1 )  ?  " Sospendi" : "Riattiva"?></a> </td>
                <?php

                if($row['Stato'] == 1){
                ?>
                    <td>  <a href="dipendenti.php?edit=<?php echo $row['id'] ?>"> Cambia ruolo  </a> </td>

                    <?php

                }
                }
                    ?>

                <?php
                }

                ?>


        </table>





        </div>

    </section>
</main>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js'></script> </script>
<script src="script.js"> </script>
</body>
</html>
