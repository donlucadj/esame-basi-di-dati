<?php

session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");

}

require('../database.php');
if (filter_var($_GET['consegna_cliente'], FILTER_VALIDATE_INT)) {
    $consegna_cliente = $_GET['consegna_cliente'];

    $sql = 'SELECT Cliente.Ragione_Sociale,Stato_consegna, Stato_chiusura,stato  FROM  Consegna_Cliente 
inner join Cliente on Cliente.ID=Consegna_Cliente.Cliente
inner join Consegna on Consegna_Cliente.Consegna=Consegna.ID
where Consegna_Cliente.ID = ?
';


    $inventory_sql = $connect->prepare($sql);
    $inventory_sql->bind_param('i', $consegna_cliente);
    $inventory_sql->execute();
    $results = $inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata
    $row = $results->fetch_assoc();

    $sql_clients = "SELECT Prodotto_Consegnato.id, Prodotto_Generico.nome, Tipologia.nome as tipologia ,Colore.nome  as colore,
Dimensione.Forma,Dimensione.Misura,Quantità,Tipo,colli
FROM `Prodotto_Consegnato`
inner join Consegna_Cliente on
Prodotto_Consegnato.Consegna_Cliente=Consegna_Cliente.ID
inner join Prodotto on Prodotto.id =Prodotto_Consegnato.Prodotto
inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto.Prodotto_Generico
inner join Cliente on Cliente.id=Consegna_Cliente.Cliente
inner join Tipologia on Tipologia.id=Prodotto.Tipologia
inner join Colore on Colore.id=Prodotto.Colore
inner join Dimensione on Dimensione.id=Prodotto.Dimensione
inner join Categoria on Categoria.id=Prodotto.Categoria
where Consegna_Cliente.Id =?";
    $clients = $connect->prepare($sql_clients);
    $clients->bind_param('i', $consegna_cliente);
    $clients->execute();
    $prodotti_res = $clients->get_result();

        ?>
        <head>


            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
            <link rel="stylesheet" href='../index.css'>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
            <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
            <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

            <link href="../index.css" rel="stylesheet">


            <link href="account.css" rel="stylesheet">
        </head>
        <body>


        <nav class="main_nav">
            <ul>
                <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
                <li> <a href="logout.php"> Log Out </a></li>
            </ul>
        </nav>
        <main>
            <aside>

                <nav class="aside_nav">
                    <ul>
                        <li> <a href="dipendenti.php" class=" aside_menu "> Dipendenti   </a></li>
                        <li> <a href="prodotti.php" class="aside_menu  "> Prodotti </a></li>
                        <li> <a href="inventario.php" class="aside_menu  "> Inventario</a></li>
                        <li> <a href="noleggio.php" class="aside_menu account"> Clienti e Noleggio</a></li>
                        <li> <a href="consegna.php" class="aside_menu account active"> Consegne</a></li>
                        <li> <a href="ritiro.php" class="aside_menu account"> Ritiri</a></li>
                    </ul>

                </nav>

            </aside>
            <section class="results">
                <h1> Consegna cliente detail</h1>

                <a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
                <table class="table_main">
                    <tr class='row_header'>
                        <th> Cliente</th>
                        <th> Operazione</th>
                        <th> Stato Consegna</th>
                        <th> Legenda</th>
                    </tr>



                    <tr class='row'>
                        <td><?php  echo $row['Ragione_Sociale'];?></td>
                        <?php
                        /* Se non vi sono nessun prodotto non posso chiudere la consegna_cliente */
                        if($prodotti_res->num_rows  == 0){
                            ?>
                            <td>Nessuna</td>
                            <td> Aperta</td>
                            <td> E' necessario inserire almeno un prodotto consegnato</td>
                            <?php

                        }
                        if($row['Stato_consegna']== 0 & $row['Stato_chiusura'] == 1 && $row['stato'] == 1  && $prodotti_res->num_rows >0 )
                        {

                            ?>

                            <td>Aperta</td>
                            <td>Chiudendo la consegna non è più possibile modificarla</td>
                            <?php
                        }

                        if($row['Stato_consegna']== 0 && $row['Stato_chiusura'] == 0 && $row['stato'] == 1)
                        {
                            ?>
                            <td>Nessuna</td>
                            <td> Chiuso</td>

                            <?php

                        }
                        if($row['Stato_consegna']== 0 & $row['Stato_chiusura'] == 0 && $row['stato'] == 0 ) {

                            ?>

                            <td> <a href="../stato_consegnata.php?consegna_cliente=<?php echo $consegna_cliente?>"> Cambia stato consegna</a></td>
                            <td> Consegna non effettuata</td>
                            <?php
                        }
                        if($row['Stato_consegna']== 1 & $row['Stato_chiusura'] == 0 && $row['stato'] == 0 ) {

                            ?>

                            <td> Nessuna </td>
                            <td> effettuata</td>
                            <?php
                        }
                        ?>
                    </tr>

                    <?php

                    ?>

                </table>
                <table class="table_main">
                    <tr class="row_header">
                        <th>Nome</th>
                        <th>Tipologia</th>
                        <th>Colore</th>
                        <th>Forma</th>
                        <th>Misura</th>
                        <th> Quantità</th>
                        <th>Tipo</th>
                        <th> Colli</th>







                    </tr>
                    <?php

                        while($prodotto=$prodotti_res->fetch_assoc()){
                            ?>
                            <tr>
                                <td><?php  echo $prodotto['nome'];?></td>
                                <td><?php  echo $prodotto['colore'];?></td>
                                <td><?php  echo $prodotto['tipologia'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Forma'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Misura'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Quantità'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['Tipo'];?></td>
                                <td class='total_product_value'><?php  echo $prodotto['colli'];?></td>
                            </tr>
                            <?php



                    }

                    ?>
                </table>
        </body>
        </html>
        <?php

}
?>


