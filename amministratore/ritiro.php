<?php

session_start();
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");

}

require('../database.php');




$sql='SELECT Ritiro.ID,Data, Ora, Percorso, concat(Dipendente.nome, " ",Dipendente.cognome) as dipendente, sum(Ritiro_Cliente.Stato_chiusura)as stato from Ritiro
    inner join Dipendente on Dipendente.id= Ritiro.Dipendente 
    left join Ritiro_Cliente on Ritiro_Cliente.Ritiro=Ritiro.ID group BY Ritiro';
//$inventory_sql=$connect->query($sql);
$inventory_sql=$connect->query($sql);
//print_r($inventory_sql->fetch(PDO::FETCH_ASSOC));
?>
<head>


    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>
<body>


<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="dipendenti.php" class=" aside_menu "> Dipendenti   </a></li>
                <li> <a href="prodotti.php" class="aside_menu  "> Prodotti </a></li>
                <li> <a href="inventario.php" class="aside_menu  "> Inventario</a></li>
                <li> <a href="noleggio.php" class="aside_menu account"> Clienti e Noleggio</a></li>
                <li> <a href="consegna.php" class="aside_menu account "> Consegne</a></li>
                <li> <a href="ritiro.php" class="aside_menu account active"> Ritiri</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
        <h1> Ritiri</h1>
        <table class="table_main">
            <tr class='row_header'>
                <th> Id </th>
                <th> Data</th>
                <th>Ora</th>
                <th>Percorso</th>
                <th>dipendente</th>
                <th> Clienti</th>
                <th> Stato Consegna</th>
            </tr>
            <?php
            while($row=$inventory_sql->fetch_assoc()){

                ?>
                <tr class='row'>
                    <td><?php  echo $row['ID'];?></td>
                    <td><?php echo  date("d/m/Y", strtotime($row['Data']))?></td>
                    <td><?php  echo $row['Ora'];?></td>
                    <td><?php  echo $row['Percorso'];?></td>
                    <td><?php  echo $row['dipendente'];?></td>
                    <td> <a href='ritiro_detail.php?ritiro=<?php echo $row['ID']?>' >view </a> </td>

                    <?php

                    if($row['stato'] == 0){
                        ?>
                        <th>  Ritiro Chiusa</a></th>
                        <?php
                    }
                    else{
                        ?>
                        <td> Ritiro Aperto</td>

                        <?php
                    }
                    ?>
                </tr>


                <?php


            }

            ?>
        </table>
    </section>


