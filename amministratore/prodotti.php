<?php

session_start();
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='AMMINISTRATORE'  )) {

    header("Location:login.php");

}

require('../database.php');

$sql='SELECT Prodotto.id, Prodotto_Generico.nome , Tipologia.nome as Tipologia, Categoria.Nome as Categoria, Colore.nome as colore, Dimensione.Forma, Dimensione.Misura,
       impacchettamento_standard FROM `Prodotto` 
    inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto_Generico inner join Tipologia on Tipologia.id=Tipologia inner join Colore on Colore.id=Colore 
    inner join Dimensione on Dimensione.id=Dimensione inner join Categoria on Categoria.id=Categoria;';
//$inventory_sql=$connect->query($sql);
$inventory_sql=$connect->query($sql);
//print_r($inventory_sql->fetch(PDO::FETCH_ASSOC));
?>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">

</head>
<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="dipendenti.php" class=" aside_menu "> Dipendenti   </a></li>
                <li> <a href="prodotti.php" class="aside_menu  active"> Prodotti </a></li>
                <li> <a href="inventario.php" class="aside_menu "> Inventario</a></li>
                <li> <a href="noleggio.php" class="aside_menu account">Clienti  e Noleggio</a></li>
                <li> <a href="account" class="aside_menu account"> Consegne</a></li>
                <li> <a href="account" class="aside_menu account"> Ritiri</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">


<table class="table_main">
    <tr class='row_header'>

        <th> Id</th>
        <th>Nome</th>
        <th>Colore</th>
        <th>Tipologia</th>
        <th>Categoria</th>
        <th>Forma</th>
        <th>Misura</th>
        <th> impacchettamento</th>


    </tr>
    <?php
    while($row=$inventory_sql->fetch_assoc()){

        ?>
        <tr class='row' data-id='<?php echo $row['id'];?>'>
            <td> <?php echo $row['id']?></td>
            <td><?php  echo $row['nome'];?></td>
            <td><?php  echo $row['colore'];?></td>
            <td><?php  echo $row['Tipologia'];?></td>
            <td><?php  echo $row['Categoria'];?></td>
            <td class='total_product_value'><?php  echo $row['Forma'];?></td>
            <td class='total_product_value'><?php  echo $row['Misura'];?></td>

            <td class='total_product_value'><?php  echo $row['impacchettamento_standard'];?></td>
        </tr>


        <?php


    }

    ?>


</table>
    </section>
<div class='form_content'>


</div>
<script src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script src='index.js'> </script>
</body>