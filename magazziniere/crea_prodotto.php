<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='MAGAZZINIERE'  )) {

    header("Location:login.php");

}
require("../database.php");

$sql_generico="SELECT * FROM Prodotto_Generico";

$prodotto_generico=$connect->query($sql_generico);

$sql_tiplogia="SELECT * FROM Tipologia";
$tipologia=$connect->query($sql_tiplogia);
$sql_dimensione="SELECT * FROM Dimensione";
$dimensione=$connect->query($sql_dimensione);
$sql_colore="SELECT * FROM Colore";
$colore=$connect->query($sql_colore);
$sql_categoria="SELECT * FROM Categoria";
$categoria=$connect->query($sql_categoria);
?>
<html>

<head>
    <link rel="stylesheet" href="magazzino.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
    <link rel="stylesheet" href="../cliente.css">
</head>

<body>

<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="prodotti.php" class=" aside_menu active "> Prodotti  </a></li>
                <li> <a href="prodotti.php" class="aside_menu  "> Caratteristiche prodotti</a></li>
                <li> <a href="magazzino.php.php" class="aside_menu  "> Magazzino</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
<h1> Inserimento Prodotti</h1>
        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
<form action="prodotto_check.php" method="post">

    <select name="prodotto_generico" required>
        <option > Seleziona Prodotto Generico</option>
        <?php

        while($row=$prodotto_generico->fetch_assoc()){

            ?>
            <option value="<?php  echo $row['id']?>"> <?php  echo $row['nome'] ?>
            </option>
            <?php

        }
        ?>

    </select>
    <select name="tipologia" required>
        <option>  Seleziona tipologia</option>
        <?php
        while($m=$tipologia->fetch_assoc()){
            ?>

            <option value="<?php echo $m['id']?>"> <?php echo $m['nome'] ?></option>
            <?php
        }

        ?>


    </select>

    <select name="dimensione" required>
        <option>  Seleziona Dimensione: Forma e Misura</option>
        <?php
        while($dim=$dimensione->fetch_assoc()){
            ?>

            <option value="<?php echo $dim['id']?>"> <?php echo $dim['Forma']." ".$dim['Misura'] ?></option>
            <?php
        }

        ?>


    </select>


    </select>

    <select name="colore" required>
        <option>  Seleziona Colore</option>
        <?php
        while($col=$colore->fetch_assoc()){
            ?>

            <option value="<?php echo $col['id']?>"> <?php echo $col['nome'] ?></option>
            <?php
        }

        ?>


    </select>
    <select name="categoria" required>
        <option>  Seleziona Categoria</option>
        <?php
        while($cat=$categoria->fetch_assoc()){
            ?>

            <option value="<?php echo $cat['id']?>"> <?php echo $cat['Nome'] ?></option>
            <?php
        }

        ?>


    </select>
    <label> Impacchettamento Standard </label>
    <input type="number" name="impacchettamento_standard">
    <input type="submit" name="submit" required>
</form>
</body>
</html>