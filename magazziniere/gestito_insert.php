<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (empty($_SESSION['log']) && $_SESSION['ruolo'] !='MAGAZZINIERE'  ) {

    header("Location:login.php");

}
require("../database.php");
$sql="SELECT Prodotto.id, Prodotto_Generico.nome , 
 Tipologia.nome as Tipologia, Colore.nome as colore, Dimensione.Forma,
 Dimensione.Misura FROM `Prodotto` inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto_Generico
  inner join Tipologia on Tipologia.id=Tipologia 
     inner join Colore on Colore.id=Colore 
inner join Dimensione on Dimensione.id=Dimensione";

$stm=$connect->query($sql);

$sql_magazzino="SELECT * FROM Magazzino";
$magazzino=$connect->query($sql_magazzino);
?>
<html>

<head>
    <link rel="stylesheet" href="magazzino.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
    <link rel="stylesheet" href="../cliente.css">
</head>

<body>

<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="prodotti.php" class=" aside_menu active "> Prodotti  </a></li>
                <li> <a href="prodotti.php" class="aside_menu  "> Caratteristiche prodotti</a></li>
                <li> <a href="magazzino.php.php" class="aside_menu  "> Magazzino</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>
            </ul>

        </nav>

    </aside>
    <section class="results">
        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
    <h1> Inserimento Prodotti nei magazzini</h1>
    <form action="gestito_check.php" method="post">

        <select name="prodotto" required>
            <option value=""> Seleziona Prodotto</option>
            <?php

            while($row=$stm->fetch_assoc()){

                ?>

                <option value="<?php  echo $row['id']?>"> <?php  echo $row['nome']." ".$row['Tipologia']." ".
                        $row['colore']." ".$row['Forma']." ".$row['Misura']
                    ?> </option>
                <?php

            }
            ?>
        </select>
        <select name="magazzino" required>
            <option value=""> Seleziona Magazzino</option>
            <?php
            while($m=$magazzino->fetch_assoc()){
                ?>

                <option value="<?php echo $m['id']?>"> <?php echo $m['Nome'] ?></option>
                <?php
            }

            ?>


        </select>
        <input type="number" name="quantity">
        <input type="submit" name="submit" required>
    </form>

    </section>
</body>
</html>
