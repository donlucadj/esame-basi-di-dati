<?php
session_start();
require ("../database.php");

if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='MAGAZZINIERE'  )) {

    header("Location:login.php");

}
if(isset($_POST['submit'])) {


    if (filter_var($_POST['prodotto_generico'], FILTER_VALIDATE_INT) &&
        filter_var($_POST['tipologia'], FILTER_VALIDATE_INT) &&
        filter_var($_POST['dimensione'], FILTER_VALIDATE_INT) &&
        filter_var($_POST['colore'], FILTER_VALIDATE_INT) &&
        filter_var($_POST['categoria'], FILTER_VALIDATE_INT) &&
        filter_var($_POST['impacchettamento_standard'], FILTER_VALIDATE_INT)
    )
    {
        $prodotto_generico = $_POST['prodotto_generico'];
        $tipologia = $_POST['tipologia'];
        $colore = $_POST['colore'];
        $categoria = $_POST['categoria'];
        $dimensione = $_POST['dimensione'];
        $impacchettamento_standard=$_POST['impacchettamento_standard'];


          $sql_insert=" INSERT INTO  Prodotto (Prodotto_Generico,Tipologia,
                        Dimensione,Colore, Categoria,impacchettamento_standard)
                        VALUES (?,?,?,?,?,?)";

           $insert=$connect->prepare($sql_insert);
           $insert->bind_param('iiiiii',$prodotto_generico,$tipologia,$dimensione,$colore,$categoria,$impacchettamento_standard);
           $insert->execute();


           if($insert->affected_rows !=1){
  echo "<p> Errore Prodotto già inserito! </p>";
           }
       }

       else{

           echo "<p> Prodotto inserito correttamente </p>";
       }
?>
<a href="prodotti.php">Ritorna ai prodotti! </a>
<?php


}
?>