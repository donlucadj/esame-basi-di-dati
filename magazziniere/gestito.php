<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='MAGAZZINIERE'  )) {

    header("Location:login.php");
exit(0);
}
require("../database.php");
$sql="SELECT Prodotto.id,Prodotto_Generico.nome , Categoria.Nome as categoria, Tipologia.nome as Tipologia, Colore.nome as colore, Dimensione.Forma, Dimensione.Misura, Disponibilità, Magazzino.Nome as nome_magazzino , DATE_FORMAT(Data, '%d-%m-%Y-%T') as Data, Magazzino.Id as magazzino_id FROM `Prodotto` inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto_Generico inner join Tipologia on Tipologia.id=Tipologia inner join Colore on Colore.id=Colore inner join Dimensione on Dimensione.id=Dimensione inner join Categoria on Categoria.id=Categoria inner join Gestito on Gestito.Prodotto=Prodotto.id 
    inner join Magazzino on Gestito.Magazzino=Magazzino.Id";
$inventory_sql=$connect->query($sql);
?>

<html>
<head>
    <link rel="stylesheet" href="product_detail.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
</head>

<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="prodotti.php" class=" aside_menu  "> Prodotti  </a></li>
                <li> <a href="caratteristiche_prodotti.php" class="aside_menu  "> Caratteristiche prodotti</a></li>
                <li> <a href="magazzino.php" class="aside_menu  "> Magazzino</a></li>
                <li> <a href="gestito.php" class="aside_menu account active">Inventario</a></li>

            </ul>

        </nav>

    </aside>
    <section class="results">
        <button> <a href="gestito_insert.php">Inserisci prodotto  in magazzino</a> </button>
        <table class="table_main">
            <tr class='row_header'>

                <th> Prodotto Id</th>
                <th>Nome</th>
                <th>Categoria</th>
                <th>Tipologia</th>
                <th>Colore</th>
                <th>Forma</th>
                <th>Misura</th>
                <th> Disponibilità</th>
                <th> Magazzino</th>
                <th> Data</th>
                <th> Aggiorna Quantità</th>

            </tr>



            <?php
            while($row=$inventory_sql->fetch_assoc()){
//while ($row=$inventory_sql->fetch_assoc()){
                ?>
                <tr class='row'>
                    <td><?php  echo $row['id'];?></td>
                    <td><?php  echo $row['nome'];?></td>
                    <td><?php  echo $row['categoria'];?></td>
                    <td><?php  echo $row['Tipologia'];?></td>
                    <td><?php  echo $row['colore'];?></td>
                    <td class='total_product_value'><?php  echo $row['Forma'];?></td>
                    <td class='total_product_value'><?php  echo $row['Misura'];?></td>
                    <td class='total_product_value'><?php  echo $row['Disponibilità'];?></td>
                    <td class='total_product_value' data-id="<?php echo $row['magazzino_id'] ?>"><?php  echo $row['nome_magazzino'];?></td>
                    <td class='total_product_value'><?php  echo $row['Data'];?></td>
                    <td><a href="gestito_update.php?magazzino_id=<?php echo $row['magazzino_id']?>&prodotto_id=<?php echo $row['id'] ?>">Update</a> </td>
                </tr>


                <?php


            }

            ?>


        </table>
        <div class='form_content'>


        </div>
    </section>
</body>
</html>
