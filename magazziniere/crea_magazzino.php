<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (!(isset($_SESSION['log']) && $_SESSION['ruolo'] =='MAGAZZINIERE'  )) {

    header("Location:login.php");

}
require('../database.php');
?>
<html>
<head>
    <link rel="stylesheet" href="product_detail.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
    <style>

        form{
            display: flex;
            flex-direction: column;

            justify-content: center;
            align-items: center;
            height: 200px;
        }
        input{
            width:20%;
            margin-bottom: 3em;
        }
        label{
            margin-bottom: 1em;
        }

        input[type="submit"]{

            width:10%;
        }
    </style>
</head>

<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="prodotti.php" class=" aside_menu  "> Prodotti  </a></li>
                <li> <a href="caratteristiche_prodotti.php" class="aside_menu "> Caratteristiche prodotti</a></li>
                <li> <a href="magazzino.php" class="aside_menu  active "> Magazzino</a></li>
                <li> <a href="gestito.php" class="aside_menu account">Inventario</a></li>

            </ul>

        </nav>

    </aside>

    <section class="results">

        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?> "> <span class="material-icons return_back">arrow_back</span> </a>
<h1> Inserimento Magazzino</h1>

<form  action="magazzino_check.php" method="POST">
    <label for="nome"> Nome</label>
    <input type="text" id="nome"   name="nome" required>
    <input type="submit" name="submit">

</form>
</section>
</html>

