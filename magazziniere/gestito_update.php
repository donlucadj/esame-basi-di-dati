<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (empty($_SESSION['log']) && $_SESSION['ruolo'] !='MAGAZZINIERE'  ) {

    header("Location:login.php");

}
require('../database.php');
if(isset($_GET['prodotto_id']) && isset($_GET['magazzino_id'])

) {


    if (filter_var($_GET['prodotto_id'], FILTER_VALIDATE_INT) &&
        filter_var($_GET['magazzino_id'], FILTER_VALIDATE_INT)
    ) {
        $prodotto=$_GET['prodotto_id'];
$magazzino=$_GET['magazzino_id'];
        $sql = "SELECT Prodotto.id,Prodotto_Generico.nome , Categoria.Nome as categoria, Tipologia.nome as Tipologia, Colore.nome as colore, Dimensione.Forma, Dimensione.Misura, Disponibilità, Qty_Totale,Magazzino.Nome as nome_magazzino ,
       Magazzino.Id as magazzino_id,qty_incr_decr, DATE_FORMAT(Data, '%d-%m-%Y-%T') as Data, Qty_scartata FROM `Gestito`
           inner join Prodotto on Gestito.Prodotto=Prodotto.id
    inner join Prodotto_Generico on Prodotto_Generico.id=Prodotto.Prodotto_Generico 
    inner join Tipologia on Tipologia.id=Prodotto.Tipologia 
    inner join Colore on Colore.id=Prodotto.Colore 
    inner join Dimensione on Dimensione.id=Prodotto.Dimensione 
inner join Categoria on Categoria.id=Prodotto.Categoria
    inner join Magazzino on Gestito.Magazzino=Magazzino.Id
where Gestito.Prodotto = ? and Gestito.Magazzino = ?
";




        $inventory_sql = $connect->prepare($sql);
        $inventory_sql->bind_param('ii',$prodotto,$magazzino);
        $inventory_sql->execute();
        $results=$inventory_sql->get_result(); // ottiene un set di risultati dall'istruzione preparata
        $row=$results->fetch_assoc();

    }
}
else{
    header("Location: noleggio_view.php");
}
?>
<head>
    <link rel="stylesheet" href="product_detail.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='index.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:ital,wght@0,200;0,300;1,200&family=Roboto:ital,wght@0,100;0,400;1,100;1,300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link href="../index.css" rel="stylesheet">


    <link href="account.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href='magazzino_update.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha512-9usAa10IRO0HhonpyAIVpjrylPvoDwiPUiKdWk5t3PyolY1cOd4DSE0Ga+ri4AuTroPR5aQvXU9xC6qOPnzFeg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;1,100;1,200;1,300;1,400;1,500&display=swap" rel="stylesheet">
</head>
<body>
<nav class="main_nav">
    <ul>
        <li> <a> Welcome  <?php echo $_SESSION['nome'];?>    </a> </li>
        <li> <a href="logout.php"> Log Out </a></li>
    </ul>
</nav>
<main>
    <aside>

        <nav class="aside_nav">
            <ul>
                <li> <a  href="prodotti.php" class=" aside_menu  "> Prodotti  </a></li>
                <li> <a href="caratteristiche_prodotti.php" class="aside_menu  "> Caratteristiche prodotti</a></li>
                <li> <a href="magazzino.php" class="aside_menu  "> Magazzino</a></li>
                <li> <a href="gestito.php" class="aside_menu account active">Inventario</a></li>

            </ul>

        </nav>

    </aside>
    <section class="results">
<h1> Aggiornamento prodotto magazzino</h1>
        <a href="gestito.php"> <span class="material-icons return_back">arrow_back</span> </a>
<table class="table_main">
    <tr class='row_header'>


        <th>Nome</th>
        <th>Categoria</th>
        <th>Tipologia</th>
        <th>Colore</th>
        <th>Forma</th>
        <th>Misura</th>
        <th> Disponibilità</th>
<th> Quantità Totale</th>
<th> Quantità incr/decr</th>
        <th> Data</th>
        <th> Quantità scartata</th>
    </tr>




        <tr class='row'>

            <td><?php  echo $row['nome'];?></td>
            <td><?php  echo $row['categoria'];?></td>
            <td><?php  echo $row['Tipologia'];?></td>
            <td><?php  echo $row['colore'];?></td>
            <td class='total_product_value'><?php  echo $row['Forma'];?></td>
            <td class='total_product_value'><?php  echo $row['Misura'];?></td>
            <td class='total_product_value'><?php  echo $row['Disponibilità'];?></td>
            <td class='total_product_value'><?php  echo $row['Qty_Totale'];?></td>
            <td class='total_product_value'><?php  echo $row['qty_incr_decr'];?></td>
            <td class='total_product_value'><?php  echo $row['Data'];?></td>
            <td class='total_product_value'><?php  echo $row['Qty_scartata'];?></td>
        </tr>





</table>
<div class="form_wrapper">

<form method="post" action="gestito_update_check.php">

    <label> Inserisci l'incremento</label>
    <input type="number" name="quantity" min="1" value="">
    <input type="hidden" name="prodotto_id" value="<?php echo $prodotto ?>">
    <input type="hidden" name="magazzino_id" value="<?php echo $magazzino ?>">
    <input type="submit" name="update_increase">
</form>


    <form method="post" action="gestito_update_check.php">

        <label> Inserisci i prodotti scartati</label>
        <input type="number" name="quantity" min="1" value="">
        <input type="hidden" name="prodotto_id" value="<?php echo $prodotto ?>">
        <input type="hidden" name="magazzino_id" value="<?php echo $magazzino ?>">
        <input type="submit" name="update_scarto" value="update_scarto">
    </form>

</div>
    </section>
    </main>
</body>




