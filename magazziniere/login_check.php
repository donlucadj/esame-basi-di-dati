<?php
ini_set ('display_errors', 1);
ini_set ('display_startup_errors', 1);
error_reporting (E_ALL);

require ("../database.php");

session_start();
$token_regex='/^[0-9a-zA-Z]+$/';

if(preg_match($token_regex,$_POST['token']) &&
    $_SESSION['token']==$_POST['token']) {


    $emailRegex = '/^[\w \.]{2,30}@[\w]+\.[a-z]+$/';
    $PasswordRegex = '/^[a-z+ A-Z+  0-9+ d+ @* -* $*]{2,30}$/';

    if (isset($_POST['email']) && isset($_POST['email']) &&
        preg_match($emailRegex, $_POST['email'])
        && preg_match($PasswordRegex, $_POST['password'])) {
        $email = $_POST['email'];
        $password = $_POST['password'];


        $query = "SELECT Nome,ruolo,password,Stato FROM Dipendente WHERE  email = ?";
        $s = $connect->prepare($query);

        $s->bind_param('s', $email);
        $s->execute();

        $get_res = $s->get_result();
        $result = $get_res->fetch_assoc();

        if ($result['ruolo'] == 'MAGAZZINIERE' && $result['Stato'] == 1) {



             if( password_verify($password,$result['password'])){



             $_SESSION['log']=true;

             $_SESSION['email']=$email;
             $_SESSION['nome']=$result['Nome'];
             $_SESSION['ruolo']=$result['ruolo'];
             unset($_SESSION['token']);
             session_regenerate_id();
             header("Location:prodotti.php");
                }

             else{
                 echo "password error";
                 http_response_code(401);
                 header("Location:login.php");
             }


        }else{

            echo "Utente non autorizzato!";
            header("Location:login.php");
        }
    }
    } else {
        echo "token error";
        http_response_code(401);
    header("Location:login.php");
    }

?>